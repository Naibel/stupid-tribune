<?php if(User::isLogged()){ 
?>
	<span class="sous_titre" style="font-size:24px;">Hey, <?php $this->pv('user_username'); ?> ! Si t'as une question à soumettre, vas-y !</span><br/><br/>
		
	<form action="<?php $this->pv('current_url'); ?>" method="post">

		<span class="sous_titre_2">Question à ajouter :</span>
		<input class="champ_blanc" type="text" name="question_text" required></br></br></br></br>

		<span class="sous_titre_2">Catégorie de la question :</span>
		<select class="champ_blanc" name="question_category">
			<?php
				
				$list_categ=Admin::listCateg();

				foreach ($list_categ as $key => $value) {
			?>
					<option value="<?php echo $list_categ[$key]['categorytext']; ?>"><?php echo $list_categ[$key]['categorytext']; ?></option> 
			<?php
				}
			?>
		</select>

		<input class="button" type="submit" name="questionsend" value="Envoyer"><br/><br/>

	</form>

	<span style="font-size:18px; color:#550000;"><a href="<?php $this->pv('home_url'); ?>">Revenir à l'accueil du back-office</a></span>
<?php } else { ?>
    <span class="sous_titre_2">Vous devez être administrateur pour acceder à cette partie du site.</span></br></br>
<?php } ?>