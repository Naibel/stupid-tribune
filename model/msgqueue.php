<?php

class MsgQueue extends Model
{
    private static $sidx = 'msg';  // Index of the SESSION
    private static $cur = 0;
    
    public static function addMessage($msg)
    {
        if(!isset($_SESSION[self::$sidx]))
            $_SESSION[self::$sidx] = array();
        $_SESSION[self::$sidx][] = $msg;
    }
    
    public static function clear()
    {
        unset($_SESSION[self::$sidx]);
        self::$cur = 0;
    }
    
    public static function getMessage()
    {
        if(!isset($_SESSION[self::$sidx][self::$cur]))
            return false;
        return $_SESSION[self::$sidx][self::$cur++];
    }
    
    public static function seek($pos = 0)
    {
        self::$cur = $pos;
    }
}