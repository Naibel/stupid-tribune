<?php

///////////////////////////////////////////////////////////
//                  ROUTES
///////////////////////////////////////////////////////////

// Index de Stupid Tribune
Router::connect(
    'index\.php',
    'home/home',
    array()
);

Router::connect(
    'activation',
    'user/activate',
    array()
);






// Exemple de route

Router::connect(
    'patate/:id/:ha',
    'home/view/:id/:ha',
    array(
        'id' => '[0-9]+',
        'ha' => '[a-z]+'
    )
);

//var_dump(Router::$routes['patate/:id/:ha']);




///////////////////////////////////////////////////////////
//                  TASKS
///////////////////////////////////////////////////////////

// Login / logout
Router::addTask('login', 'user/login');
Router::addTask('logout', 'user/logout');

// Inscription
Router::addTask('register', 'user/register');

// Upload d'avatar
Router::addTask('uploadavatar', 'user/uploadavatar');

// Commentaires
Router::addTask('commentsend', 'article/commentsend');

//Ajouter un article
Router::addTask('articlesend', 'admin/articlesend');

//Ajouter une question à la BDD
Router::addTask('questionsend', 'admin/questionsend');

//Ajouter une réponse à la BDD
Router::addTask('answersend', 'admin/answersend');

//Ajouter un sondage à la BDD
Router::addTask('pollsend', 'admin/pollsend');

// Votes
Router::addTask('vote', 'poll/vote');
