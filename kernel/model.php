<?php

class Model
{
    protected $perpetuated = false; //*< Faux si des changements sont faits, mais pas dans la BDD.
    
    // FUCK OFF MYSQL qui gère pas les transactions imbriquées. 'Culay !
    protected static $transacting = false;
    
    
    public function __construct()
    {
    }
    
    /**
     * @brief Charge le modèle $model pour pouvoir l'instancier par la suite. Un modèle ne sera jamais chargé plusieurs fois au sein d'un même controlleur, même si cette focntion esta ppelée plusieurs fois.
     * @param in string $model Parameter description.
     * @return Cette fonction vrai si le modèle a été chargé, faux sinon.
     */
    public static function loadModel($model)
    {
        $file = ROOT . MODEL_DIR . $model . '.php';
        if(!file_exists($file)) return false;
        $name = ucfirst($model);
        require_once $file;
        return true;
    }
    
    public static function beginTransaction()
    {
        if(self::$transacting) return;
        Db::get()->beginTransaction();
        self::$transacting = true;
    }
    
    public static function commitTransaction()
    {
        if(!self::$transacting) return;
        Db::get()->commit();
        self::$transacting = false;
    }
    
    public static function rollbackTransaction()
    {
        if(!self::$transacting) return;
        Db::get()->rollback();
        self::$transacting = false;
    }
    
    public static function BDDError($e, $msg = 'Une erreur est survenue, veuillez réessayer.')
    {
        Db::get()->rollback();
        if(isdebug('db'))
        {
            echo 'Erreur BDD : ', $e->getMessage();
        }
        else{
            MsgQueue::addMessage($msg);
        }
    }
}