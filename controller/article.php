<?php

class ArticleController extends Controller
{
    
    public function defaultAction()
    {
        $this->display();
    }    
    
    public function recents()
    {
        Model::loadModel('article');
        $this->createView('recents');
        
        $numarticles = 4;
        
        $articles = Article::getLastArticles($numarticles);
        //Format the first article
        if(strlen($articles[0]->articletext) > 300)
            $articles[0]->articletext =substr($articles[0]->articletext, 0, 300) . '...';
        // Format all dates
        for($i = 0 ; $i < $numarticles ; ++$i)
        {
            $articles[$i]->articledatetime = new DateTime($articles[$i]->articledatetime);
        }
        
        $this->view->set('articles', $articles);
    }
    
    public function single()
    {
        Model::loadModel('article');
        $this->createView('single');
        
        $id = intval($this->params[0]);
        if(!Article::exists($id))
        {
            return Dispatcher::ERROR404;
        }
        $article = Article::getById($id);
        $article->articledatetime = new DateTime($article->articledatetime);
        
        $this->view->set('article', $article);
    }
    
    public function show()
    {
        Model::loadModel('article');
        $this->createView('show');
        
        $id = intval($this->params[0]);
        if(!Article::exists($id))
        {
            return Dispatcher::ERROR404;
        }
        
        $this->request('article_single', 'article', 'single', array($id));
        $this->request('article_comment_form', 'article', 'comment_form', array($id));
        $this->request('article_comments', 'article', 'comments', array($id));
        
        $this->setLayout('largeLayout');
    }
    
    public function searchform()
    {
        $this->createView('searchform');
        $this->view->set('search_url', Router::url('article/search'));
    }
    
    public function search()
    {
        Model::loadModel('article');
        $this->createView('searchresult');
        
        $search = Dispatcher::inputGet('s', Dispatcher::PARAM_STR);
        if(!$search)
        {
            $this->view->set('articles', null);
        }
        else
        {

            $articles = Article::Search($search);

            foreach($articles as $article)
            {
                $article->articledatetime = new DateTime($article->articledatetime);
            }
            
            $this->view->set('articles', $articles);
        }
        
        $this->view->set('search', $search);
        
        $this->setLayout('defaultLayout');
        
    }
    
    public function comment_form()
    {
        model::loadModel('user');
        $this->createView('commentform');
        
        $id = intval($this->params[0]);
        if(!Article::exists($id))
        {
            return Dispatcher::ERROR404;
        }
        
        $this->view->set('current_url', Router::url(Request::getClientUrl()));
        $this->view->set('articleid', $id);
    }
    
    public function comments()
    {
        model::loadModel('comment');
        $this->createView('comments');
        
        $id = intval($this->params[0]);
        if(!Article::exists($id))
        {
            return Dispatcher::ERROR404;
        }
        
        $comments = Comment::getByArticle($id);
        foreach($comments as $comment)
        {
            $comment->comment_datetime = new DateTime($comment->comment_datetime);
        }
        $this->view->set('comments', $comments);
    }
    
    public function commentsend()
    {
        model::loadModel('user');
        model::loadModel('comment');
        model::loadModel('article');
        if(!User::isLogged()) return false;
        
        
        $id = Dispatcher::inputPost('commentsend_article', Dispatcher::PARAM_INT);
        $text = Dispatcher::inputPost('commentsend_content', Dispatcher::PARAM_STR);
        $text = htmlentities(substr($text, 0, 200));
        $author = User::getLogged()->username;
        
        if(!Article::exists($id))
        {
            MsgQueue::addMessage('L\'article à commenter n\'existe pas.');
            return;
        }
        
        Comment::addComment($id, $author, $text);
        
    }
    
}