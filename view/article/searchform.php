<div class="recherche">
    <form action="<?php $this->pv('search_url'); ?>" method="get">
        <span class="sous_titre" style="font-size:20px;">
            Rechercher un article
        </span>
        <div class="ligne"></div>
        <input class="champ_blanc" type="text" name="s" placeholder="Votre recherche" required>
        <input class="button" type="submit" value="Allez, zou !">
    </form>
</div>