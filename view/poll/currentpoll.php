<br/><br/>
<span class="sous_titre" style="font-size:23px;">Le sondage du jour</span>
<div class="ligne"></div>
<p class="sous_titre_2" style="color:#550000;"><?php echo $this->v('poll')->questiontext; ?></p>

<?php if(User::isLogged()) { ?>
    <?php if(!$this->v('vote')) { ?>
        <form action="<?php $this->pv('current_url'); ?>" method="post">
            <ul class="poll_ul">
            <?php foreach($this->v('poll')->answers as $answer){ ?>
            <li> 
                <input type="radio" name="vote_answernum"
                       value="<?php echo $answer->answernum; ?>">
                <span><?php echo $answer->answertext; ?></span>
            </li>
            <?php } ?>
            <input type="hidden" name="vote_poll"
                   value="<?php echo $this->v('poll')->pollid; ?>">
            <input class="button" type="submit" name="vote" value="Répondre">
        </form>
    <?php } 
            else 
            { 
                $results=Poll::getResults($this->v('poll')->pollid);
                $allresults=Poll::getAllResults($this->v('poll')->pollid);
    ?>
        <em>Vous avez déjà voté pour ce sondage ! Merci de votre participation !</em>
        <ul class="poll_ul">
        <?php $i=1; foreach($this->v('poll')->answers as $answer){ ?>
            <?php if($this->v('vote')->answernum == $i) { ?>
            <li>
                <span>
                    <b><?php echo $answer->answertext;  ?></b><br/>
                    <?php

                        $nb_rep=0;
                        $nb_two_digits=0;

                        for($j=0 ; $j<sizeof($results) ; $j++)
                            {
                                if($results[$j]['voteanswernum'] == $answer->answernum)
                                    {
                                        $nb_rep = ($results[$j]['nbReponses'])*100/($allresults[0]['nbAllReponses']);

                                        $nb_two_digits = number_format((float)$nb_rep, 1, '.', '');
                                    }
                            }

                    ?>
                        <div class="barre_rep" style="width:<?php echo $nb_two_digits; ?>%;"></div><?php echo  $nb_two_digits; ?> %
                </span>
            </li>
            <?php } else { ?>
            <li>
                <span>
                    <?php echo $answer->answertext; ?>
                    <?php

                        $nb_rep=0;
                        $nb_two_digits=0;

                        for($j=0 ; $j<sizeof($results) ; $j++)
                            {
                                if($results[$j]['voteanswernum'] == $answer->answernum)
                                    {
                                        $nb_rep = ($results[$j]['nbReponses'])*100/($allresults[0]['nbAllReponses']);

                                        $nb_two_digits = number_format((float)$nb_rep, 1, '.', '');
                                    }
                            }
                            
                    ?>
                         <div class="barre_rep" style="width:<?php echo $nb_two_digits; ?>%; opacity:0.6;"></div><?php echo  $nb_two_digits; ?> %
                </span>
            </li>
            <?php } ?>
        <?php ++$i; } ?>
        <span>Nombre de votants : <b><?php echo $allresults[0]['nbAllReponses']; ?></b><br/></span>
    <?php } ?>
        </ul>
<?php } else { ?>
    <em style="font-size:14px;">Veuillez vous connecter pour participer à ce sondage !</em><br/><br/>
<?php } ?>

