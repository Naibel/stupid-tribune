<span class="sous_titre">Résultats de la recherche : </span>
<span class="sous_titre_2" style="font-size:24px;"><?php $this->pv('search'); ?></span></br></br>

<?php if(empty($this->v('articles'))){ ?>
    <span class="sous_titre">Désolé ! On a rien pu trouver !</span><br/><br/>
	<span class="sous_titre_2">C'est ballot, je vous l'accorde...</span>
<?php } else { ?>
    <?php foreach($this->v('articles') as $article){ ?>
        <a style="color:#550000;" href="<?php echo $article->articleurl; ?>">
			<span class="sous_titre"><?php echo $article->articletitle; ?></span>
		</a><br/>
		<span class="time_arch">
			<?php echo $article->articledatetime->format('d/m/Y - H:i'); ?>
		</span><br/>
		<em>par <?php echo $article->articleauthor; ?></em><br/>
		<div class="ligne"></div>
    <?php } ?>
<?php } ?>