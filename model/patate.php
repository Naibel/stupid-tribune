<?php

class Patate extends Model
{
    public static $table = 'patates';
    
    public $patateid;
    public $patatenom;
    public $patatetaille;
    
    public function __construct($content = null)
    {
        parent::__construct();
    }
    
    /**
     * @brief Retourne un objet Patate récupéré depuis la BDD par son id.
     * @param in int $id L'id de la patate à récupérer.
     * @return retourne la Patate récupérée, null sinon.
     */
    public static function getById($id)
    {
        $obj = new Patate();
        $sql = 'SELECT * FROM `'.self::$table.'` WHERE `patateid` = :id';
        $req = Db::get()->prepare($sql);
        $req->bindValue(':id', $id, PDO::PARAM_INT);
        $req->setFetchMode(PDO::FETCH_INTO, $obj);
        $req->execute();
        $req->fetch();

        return $obj;
    }
    
    public function setNom($nom)
    {
        $this->patatenom = $nom;
        $this->perpetuated = false;
    }
    
}