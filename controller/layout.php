<?php

class LayoutController extends Controller
{

    public function defaultAction()
    {
        $this->defaultLayout();
    }
    
    public function defaultLayout()
    {
        $this->createView('default');
        
        $this->view->set('css_url', Router::url('design/style.css'));
        $this->view->set('jquery_url', Router::url('js/jquery-1.9.1.min.js'));
        $this->view->set('mainscript_url', Router::url('js/script.js'));
        
        $this->request('header', 'header');
        $this->request('mainmenu', 'mainmenu');
        $this->request('userpanel', 'user', 'userpanel');
        $this->request('msgqueue', 'msgqueue');
        $this->request('searchform', 'article', 'searchform');
        $this->request('currentpoll', 'poll', 'currentpoll');
        $this->request('footer', 'footer');
        
    }
    
    public function largeLayout()
    {
        $this->createView('large');
        
        $this->view->set('css_url', Router::url('design/style.css'));
        $this->view->set('jquery_url', Router::url('js/jquery-1.9.1.min.js'));
        $this->view->set('mainscript_url', Router::url('js/script.js'));
        
        $this->request('header', 'header');
        $this->request('mainmenu', 'mainmenu');
        $this->request('userpanel', 'user', 'userpanel');
        $this->request('msgqueue', 'msgqueue');
        $this->request('footer', 'footer');
    }

    public function adminLayout()
    {
        $this->createView('admin');
        
        $this->request('msgqueue_admin', 'msgqueue');

        $this->view->set('css_url', Router::url('design/style.css'));
        $this->view->set('jquery_url', Router::url('js/jquery-1.9.1.min.js'));
        $this->view->set('mainscript_url', Router::url('js/script.js'));
        $this->view->set('tiny_mce_url', Router::url('tinymce/tinymce.min.js'));
    }
}