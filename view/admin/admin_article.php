<?php if(User::isLogged()){ ?>
	<span class="sous_titre" style="font-size:24px;">Hey, <?php $this->pv('user_username'); ?> ! Tu désires rédiger un article ? Bah, vas-y !</span><br/><br/>
		
		<form action="<?php $this->pv('current_url'); ?>" method="post">

			<span class="sous_titre_2">Titre de l'article :</span>
			<input class="champ_blanc" type="text" name="article_title"><br/><br/><br/><br/>
			<input type="hidden" name="pseudo" value="<?php $this->pv('user_username'); ?>">

			<span class="sous_titre_2">Contenu de l'article :</span>
			<textarea id="content_textarea" name="article_content"></textarea>
			<input class="button" type="submit" name="articlesend" value="Envoyer"><br/><br/>

		</form>

	<span style="font-size:18px; color:#550000;"><a href="<?php $this->pv('home_url'); ?>">Revenir à l'accueil du back-office</a></span>
<?php } else { ?>
    <span class="sous_titre_2">Vous devez être connecté pour acceder à cette partie du site.</span></br></br>
<?php } ?>