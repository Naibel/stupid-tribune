<?php

class Comment extends Model
{
    public static $table = 'commentaires';
    
    public $comment_id;
    public $comment_article;
    public $comment_author;
    public $comment_datetime;
    public $comment_text;
    
    public function __construct($content = null)
    {
        parent::__construct();
    }
    
    /**
     * @brief Retourne un objet Patate récupéré depuis la BDD par son id.
     * @param in int $id L'id de la patate à récupérer.
     * @return retourne la Patate récupérée, null sinon.
     */
    public static function getById($id)
    {
        // TODO
    }
    
    /**
     * @brief Retourne tous les commentaire de l'article $id.
     * @param in int $id L'id de l'article duquel récupérer les commentaires.
     * @return Un tableau d'objets Comment.
     */
    public static function getByArticle($id)
    {
        try{
            Model::beginTransaction();
            
            $sql = 'SELECT * FROM `'.self::$table.'`
                    WHERE `comment_article` = :id
                    ORDER BY `comment_id` DESC';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':id', $id, PDO::PARAM_INT);
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Article', array());
            $req->execute();
            $ret = $req->fetchAll();
            
            Model::commitTransaction();
            
            return $ret;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public static function addComment($id, $author, $content)
    {
        try{
            Model::beginTransaction();
            
            $sql = 'INSERT INTO `'.self::$table.'`
                    (`comment_article`,
                     `comment_author`,
                     `comment_datetime`,
                     `comment_text`)
                    VALUES
                    (:id, :author, NOW(), :text)';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':id', $id, PDO::PARAM_INT);
            $req->bindValue(':author', $author, PDO::PARAM_STR);
            $req->bindValue(':text', $content, PDO::PARAM_STR);
            $req->execute();
            
            Model::commitTransaction();
            
            return true;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public static function getLastComments($num)
    {
        try{
            Model::beginTransaction();
        
            $sql = 'SELECT * FROM `'.self::$table.'`
                    ORDER by `comment_datetime` DESC
                    LIMIT 0, :num';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':num', $num, PDO::PARAM_INT);
            $req->execute();
            
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Comment', array());
            $ret = $req->fetchAll();
            
            Model::commitTransaction();

            return $ret;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
}