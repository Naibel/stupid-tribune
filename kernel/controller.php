<?php

/**
 * @brief Un Controlleur est un point d'entrée et le panneau de commande de chaque fonctionnalité de l'application. Il est capable d'exécuter plusieurs actions sur lui, afin de générer une vue.
 */
class Controller
{
    public $params;
    public $name;
    public $view;
    public $subs = array();
    public $layout = null;
    
    /**
     * @brief Le constructeur d'un controlleur. Initialise son nom et ses actions grâce au contenu de la requête $request.
     * @param in Request $request La requête qui a engendré le constructeur.
     * @return Cette fonction ne retourne rien.
     */
    function __construct($request = null)
    {
        if(!$request) return;
        $this->name = $request->controller;
        $this->params = $request->params;
    }
    
    /**
     * @brief Charge la vue $view dans le controlleur.
     * @param in string Le nom de la vue à charger. Ce nom doit être en minuscule et correspond au nom du fichier sans l'extension. Ce fichier doit être situé dans un sous-dossier portant le nom du constructeur.
     * @return Cette fonction ne retroune rien.
     */
    public function createView($view)
    {
        $file = ROOT . VIEW_DIR . $this->name.'/'.$view.'.php';
        //$name = ucfirst($view);
        $this->view = new View($file);
    }
    
    /**
     * @brief Génère la vue et l'affiche.
     * @return Cette fonction ne retourne rien.
     */
    public function printView()
    {
        echo $this->render();
    }
    
    /**
     * @brief Génère la vue et la renvoie. Contrairement à printView(), getView() n'affiche rien, et le résultat de la vue pourra être inclus ailleurs.
     * @return Cette fonction ne retourne rien.
     */
    public function getView()
    {
        return $this->render();
    }
    
    /**
     * @brief Fait le rendu du controlleur en utilisant la vue qui lui a été affectée avec CreateView().
     * @return Cette fonction ne retourne rien.
     */
    private function render()
    {
        if(isset($this->view))
            return $this->view->render();
        elseif(isdebug('showAllViews'))
            return $this->name.' does not have view.';
    }
    
    /**
     * @brief Charge le controlleur $ctrl comme sous-controlleur avec les paramètres $params, puis le renvoie. Il sera stockée dans le tableau memebre $subs avec la clé $key.
     * @param in string $key La clé pour accéder au sous-controlleur dans le tableau $subs.
     * @param in string $ctrl Le nom du controlleur à charger, en minuscule. Correspond au nom du fichier sans l'extension.
     * @param in array $params Un tableau deparamètres à envoyer au controlleur. Celui-ci saura quoi en faire.
     * @return Retourne le controlleur créé, false s'il n'a pas pu être créé, tout comme la fonction LoadController() de la classe Dispatcher.
     */
    public function subController($key, $ctrl, $params = array())
    {
        $controller = Dispatcher::loadController($ctrl, $params);
        if($controller) $this->subs[$key] = $controller;
        return $controller;
    }
    
    /**
     * @brief Simule une requete et la place dans le sous-controlleur $key.
     * @param in string $key La clé où stocker le sous-contrôlleur et la sous-vue.
     * @param in string $ctrl Le nom du controlleur à charger, en minuscule. Correspond au nom du fichier sans l'extension.
     * @param in string $action Le nom de l'action à exécuter.
     * @param in array $params Un tableau deparamètres à envoyer au controlleur. Celui-ci saura quoi en faire.
     * @return Cette fonction ne retroune rien.
     */
    public function request($key, $ctrl, $action = DEFAULT_ACTION, $params = array())
    {
        $this->subController($key, $ctrl, $params);
        $this->subs[$key]->$action();
        $this->view->subView($key, $this->subs[$key]);
    }
    
    /**
     * @brief Crée un layout et l'effecte au controller. Note : le layout n'est utile que s'il est placé sur le controlleur principal.
     * @param in string $layout Le nom du layout à charger. Il correspond au nom de l'action du controlleur LayoutController.
     * @param in array $params Les paramètres à envoyer au layout.
     * @return Cette fonction ne retourne rien.
     */
    public function setLayout($layout, $params = array())
    {
        $this->layout = Dispatcher::loadController(LAYOUT_CONTROLLER, $params);
        if(!$this->layout or !method_exists($this->layout, $layout)){
            
            echo 'No more layout.<br/>';
            $this->layout = null;
            return;
        }
        
        call_user_func(
            array($this->layout, $layout)
        );
        
        $this->layout->subs['content'] = $this;
        
        $this->layout->view->subView('content', $this->layout->subs['content']);
        
    }
    
}

