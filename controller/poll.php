<?php

class PollController extends Controller
{
    
    public function defaultAction()
    {
        $this->currentpoll();
    }
    
    public function currentpoll()
    {
        Model::loadModel('user');
        Model::loadModel('poll');
        $this->createView('currentpoll');
        
        $poll = Poll::getLast(true);

        $this->view->set('poll', $poll);

        if(User::isLogged())
        {
            $vote = Poll::getVote($poll, User::getLogged(), true);
            $this->view->set('vote', $vote);
            
            $this->view->set('current_url', Router::url(Request::getClientUrl()));
        }
    }
    
    public function vote()
    {
        Model::loadModel('user');
        Model::loadModel('poll');
        
        if(!User::isLogged())
        {
            MsgQueue::addMessage('Connectez-vous pour voter.');
            return;
        }
        
        $votenum = Dispatcher::inputPost('vote_answernum', Dispatcher::PARAM_INT);
        if(!$votenum)
        {
            MsgQueue::addMessage('Vous votez pour quoi déjà ?');
            return;
        }
        
        $pollid = Dispatcher::inputPost('vote_poll', Dispatcher::PARAM_INT);
        if(!Poll::exists($pollid))
        {
            MsgQueue::addMessage('Le sondage auquel vous votez n\'existe pas.');
            return;
        }
        if(Poll::getVote( Poll::getById($pollid), User::getLogged()))
        {
            MsgQueue::addMessage('Vous avez déjà voté.');
            return;
        }
        
        Poll::vote($pollid, $votenum);
        
        MsgQueue::addMessage('Votre vote a bien été pris en compte.');
        
    }
    
    
}