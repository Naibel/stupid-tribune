<footer>
	<div class="footer_wrapper">
		<div class="haut_footer">
			<div class="colonne_footer">
				<div class="colonne_wrapper">
					<div class="colonne_inner_wrapper">
						<span class="sous_titre_2">Dernières actus</span>
						<div class="ligne_blanc"></div>
						<ul>
                            <?php foreach($this->v('articles') as $article) { ?>
                                <li>
								<a style="color:#fff;"
                                   href="<?php echo $article->articleurl; ?>">
                                    <span>
                                        <?php echo $article->articletitle; ?>
                                    </span>
                                </a>
								</li>
                            <?php } ?>
                        </ul>
					</div>
				</div>
			</div>
			<div class="colonne_footer">
				<div class="colonne_wrapper">
					<div class="colonne_inner_wrapper">
                        <span class="sous_titre_2">Derniers commentaires</span>
						<div class="ligne_blanc"></div>
						<ul>
                            <?php foreach($this->v('comments') as $comment) { ?>
                                <li>
								    <span>
                                        <?php echo $comment->comment_text; ?>
                                    </span><br/>
                                    <span style='font-style:italic; font-size:10px;'>
                                        <?php echo $comment->comment_author; ?> -
                                        <?php echo $comment->comment_datetime->format('d/m/Y - H:i'); ?>
                                    </span><br/>
                                </li>
                            <?php } ?>
                        </ul>
					</div>
				</div>
			</div>
			<div class="colonne_footer">
				<div class="colonne_wrapper">
					<div class="colonne_inner_wrapper">
						<span class="sous_titre_2">Derniers sondages</span>
						<div class="ligne_blanc"></div>
                        <ul>
                            <?php foreach($this->v('polls') as $poll) { ?>
                            <li>
								<span><?php echo $poll->questiontext; ?></span></br>
				            </li>
                            <?php } ?>
                        </ul>
                    </div>
				</div>
			</div>
		</div>
		<div class="bas_footer">
			<div class="ligne_blanc"></div>
			<span class="copyright" style="text-align:center;">2015 - Belhaj/Mourot - Tous droits p'tet réservés, on sait pas en fait.</span>
		</div>
	</div> 
</footer>