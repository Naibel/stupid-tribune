<?php

class UserController extends Controller
{
    
    public function defaultAction()
    {
        // None
    }
    
    public function userpanel()
    {
        
        Model::loadModel('user');
        //User::login('thaledric', 'lemotdepasse');
        //debugn('SESSION', $_SESSION);
        
        // Si on est pas logué, on affiche le logpanel à la place.
        if(!User::isLogged())
        {
            $this->logpanel();
            return;
        }
        
        $this->createview('userpanel');
        
        // get the user values
        $user = User::getLogged();

        $user->nb_polls = User::getNbPolls($user->profile_id);

        $user->useravatar = User::getAvatar($user->profile_id);

        $this->view->set( array(
            'user_login' => $user->login,
            'user_username' => $user->username,
            'user_avatar' => $user->useravatar,
            'user_registration_date' => new DateTime($user->registration_date),
            'user_last_login' => new DateTime($user->last_login),
            'user_nbpolls' => $user->nb_polls
        ));

        $this->view->set('current_url', Router::url(Request::getClientUrl()));
        
    }
    
    public function logpanel()
    {
        $this->createview('logpanel');
        
        $this->view->set('current_url', Router::url(Request::getClientUrl()));
    }
    
    public function logout()
    {
        Model::loadModel('user');
        //debugn('Yé souis Lougout', null);
        User::logout();
    }
    
    public function login()
    {
        Model::loadModel('user');
        $login = Dispatcher::inputPost('login_login', Dispatcher::PARAM_STR);
        $password = Dispatcher::inputPost('login_password', Dispatcher::PARAM_STR);
        $succeed = User::login($login, $password);
    }
    
    public function register()
    {
        Model::loadModel('user');
        
        $login = Dispatcher::inputPost('register_login', Dispatcher::PARAM_STR);
        $password = Dispatcher::inputPost('register_password', Dispatcher::PARAM_STR);
        $email = Dispatcher::inputPost('register_email', Dispatcher::PARAM_STR);
        
        // Check validity
        
        if (!preg_match("#[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}#", $email))
        {
            MsgQueue::addMessage('L\'adresse e-mail donnée n\'est pas valide.');
            var_dump('BAD EMAIL');
            return;
        }
        
        User::register(
            $login,
            $password,
            $email
        );
    }
    
    public function activate()
    {
        Model::loadModel('user');
        
        $login = Dispatcher::inputGet('log', Dispatcher::PARAM_STR);
        $key = Dispatcher::inputGet('cle', Dispatcher::PARAM_STR);
        User::activate($login, $key);
        
        header('Location: '.Router::url(''));    
    }

    public function uploadavatar(){

        Model::loadModel('user');

        $user = User::getLogged();

        $dossier = './web/design/backgrounds/avatar/';
        $fichier = 'avatar_' . date('Y-m-d-H-i-s') . '_' . uniqid() . '.jpg';
        $taille_maxi = 1000000;
        $taille = filesize($_FILES['file_galerie']['tmp_name']);
        $extensions = array('.png', '.PNG', '.jpg', '.JPG', '.jpeg');
        $extension = strrchr($_FILES['file_galerie']['name'], '.'); 
        //Début des vérifications de sécurité...

        if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
        {
            $erreur_upload = MsgQueue::addMessage('Vous devez uploader un fichier de type png, jpg ou jpeg.');
        }
        if($taille>$taille_maxi)
        {
            $erreur_upload = MsgQueue::addMessage('Le fichier est trop gros (1 Mo maximum!).');
        }
        if(!isset($erreur_upload)) //S'il n'y a pas d'erreur, on upload
        {
            if(move_uploaded_file($_FILES['file_galerie']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
            {
                User::compress_image($dossier.$fichier, $dossier.$fichier, 70);

                User::uploadAvatar($fichier, $user->login);

                return true;

            }

            else //Sinon (la fonction renvoie FALSE).
            {
                MsgQueue::addMessage('Echec de l\'upload !');
            }
        }
        else
        {
            echo $erreur_upload;
        }
    }
}