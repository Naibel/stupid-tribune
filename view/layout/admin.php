<!DOCTYPE html>

	<head>
		<meta charset="utf-8">
		<title>Stupid Tribune - Administration</title>
		<link rel="stylesheet" href="<?php $this->pv('css_url'); ?>">
	</head>

	<body>
        <div class="content_admin">
        		<?php $this->ps('msgqueue_admin'); ?>
                <?php $this->ps('content'); ?>
        </div>
        
        <!-- Javascript -->
        
        <script src ="<?php $this->pv('jquery_url'); ?>"></script>
		<script src ="<?php $this->pv('mainscript_url'); ?>"></script>
        <script src ="<?php $this->pv('tiny_mce_url'); ?>"></script>
        <script type="text/javascript">
			tinymce.init({
			    selector: "#content_textarea"
			 });
		</script>
    </body>
</html>