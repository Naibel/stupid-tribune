<?php

/**
 * @brief Stocke la requête parsée de l'utilisateur.
 */
class Request
{
    
    public $url;
    public $controller;
    public $action;
    public $params = array();
    
    function __construct()
    {
        
    }
    
    public static function getClientRequest()
    {
        $req = new Request();
        $req->url = self::getClientUrl();
        return $req;
    }
    
    public static function getClientUrl()
    {
        return isset($_SERVER['PATH_INFO']) ? trim($_SERVER['PATH_INFO'], '/') : '';
    }
}

