<span class="sous_titre">A la une du site</span>
<div class="ligne"></div>

<a href='<?php echo $this->v('articles')[0]->articleurl; ?>' style="color:#550000;">
    <span class="sous_titre_2"><?php echo $this->v('articles')[0]->articletitle; ?></span><br/>
</a>

<span class="time_arch">
    <?php echo $this->v('articles')[0]->articledatetime->format('d/m/Y - H:i'); ?>
</span><br/>

<em>par <?php echo $this->v('articles')[0]->articleauthor; ?></em><br/>

<p>
    <?php echo $this->v('articles')[0]->articletext; ?>
</p>

<span class="sous_titre" style="font-size:20px;">Archives</span>

<?php
for ($i = 1; $i < 4; ++$i) {
?>
    <div class="ligne"></div>
    <a href="<?php echo $this->v('articles')[$i]->articleurl; ?>" style="color:#550000;"><span class="sous_titre_2"><?php echo $this->v('articles')[$i]->articletitle; ?></span><br/></a>
    <span class="time_arch"><?php echo $this->v('articles')[$i]->articledatetime->format('d/m/Y - H:i'); ?></span><br/>
    <em>par <?php echo $this->v('articles')[$i]->articleauthor; ?></em><br/>
<?php
}
?>