-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 05 Mai 2015 à 17:05
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `stupid_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `answercategories`
--

CREATE TABLE IF NOT EXISTS `answercategories` (
  `answerid` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  PRIMARY KEY (`answerid`,`categoryid`),
  KEY `CATEGORIES_ANSWERCATEGORIES_CATEGORYID` (`categoryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `answercategories`
--

INSERT INTO `answercategories` (`answerid`, `categoryid`) VALUES
(3, 1),
(4, 1),
(31, 1),
(32, 1),
(33, 1),
(1, 2),
(2, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(28, 2),
(34, 2),
(5, 4),
(6, 4),
(7, 4),
(27, 4),
(29, 4),
(30, 4),
(35, 4);

-- --------------------------------------------------------

--
-- Structure de la table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `answerid` int(11) NOT NULL AUTO_INCREMENT,
  `answertext` text NOT NULL,
  PRIMARY KEY (`answerid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Contenu de la table `answers`
--

INSERT INTO `answers` (`answerid`, `answertext`) VALUES
(1, 'Ta mère'),
(2, 'La Reine d''Angleterre'),
(3, '1984'),
(4, '42'),
(5, 'Scoualala !'),
(6, 'Sapristi !'),
(7, 'Prout'),
(8, 'Napoléon'),
(9, 'François Hollande'),
(10, 'Steve McQueen'),
(11, 'Super Mario'),
(12, 'Le chat de Shroedinger'),
(13, 'Homer Simpson'),
(14, 'Un boeuf'),
(17, 'La tour Eiffel'),
(18, 'M. Hulot'),
(19, 'Superman'),
(20, 'Herald'),
(21, 'Zygy Zwig'),
(22, 'Angela Merkel'),
(23, 'Didier Super'),
(24, 'Les chorons'),
(25, 'Batfred'),
(26, 'Morsay'),
(27, 'Funland'),
(28, 'Sadonise'),
(29, 'Biatch !'),
(30, 'Spleu !'),
(31, '-3000 av J.C'),
(32, '600000 après Céline'),
(33, '1994'),
(34, 'Les illuminatis'),
(35, 'Bend over');

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `articleid` int(11) NOT NULL AUTO_INCREMENT,
  `articleauthor` varchar(64) NOT NULL,
  `articlepoll` int(11) DEFAULT NULL,
  `articletitle` varchar(256) DEFAULT NULL,
  `articletext` text NOT NULL,
  `articledatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`articleid`),
  KEY `articleauthor` (`articleauthor`,`articlepoll`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `articles`
--

INSERT INTO `articles` (`articleid`, `articleauthor`, `articlepoll`, `articletitle`, `articletext`, `articledatetime`) VALUES
(1, 'dbelhaj', NULL, 'M. Machin toujours en liberté', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis hendrerit ultricies lectus, non volutpat magna auctor eget. Curabitur quis pharetra elit. Aenean sagittis, tortor ut molestie laoreet, lorem diam tincidunt leo, eget varius tellus quam in risus. Donec at vestibulum nulla. Nulla eget eros sed lacus vulputate pretium. Nam tincidunt elementum dictum. Praesent laoreet eget est quis tempor. Duis commodo hendrerit elementum. Integer lectus justo, semper vel lobortis quis, aliquam quis tortor.\r\n\r\nSed at nisl sed ex rhoncus molestie in nec odio. Vivamus quis vestibulum diam, egestas molestie metus. Pellentesque vitae justo eu ipsum suscipit cursus. Nullam a felis dolor. Donec varius, tortor eu maximus hendrerit, tellus libero ultricies nulla, in ullamcorper metus eros et sem. Nunc at condimentum eros, in euismod ex. Fusce ligula justo, egestas et ullamcorper a, aliquet eget libero. Integer tincidunt, enim id sodales cursus, mauris orci egestas sem, at maximus lacus eros ut lacus.\r\n\r\nVivamus cursus id dui id viverra. Mauris bibendum pharetra mauris, nec blandit justo varius at. Etiam sapien eros, bibendum ut condimentum id, lacinia sit amet libero. Donec molestie vitae eros nec tristique. Mauris tincidunt justo eget rutrum semper. Quisque fringilla aliquam magna. Aliquam a dolor maximus mauris laoreet consectetur nec eget massa. Quisque porta iaculis justo, a tincidunt dolor semper ut. Vivamus a dui tortor. Aliquam mauris est, ultrices eu justo ac, commodo tristique diam. ', '2015-03-19 09:20:35'),
(2, 'dbelhaj', NULL, 'Le chat de Shroedinger avoue : "Je suis cong !"', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis hendrerit ultricies lectus, non volutpat magna auctor eget. Curabitur quis pharetra elit. Aenean sagittis, tortor ut molestie laoreet, lorem diam tincidunt leo, eget varius tellus quam in risus. Donec at vestibulum nulla. Nulla eget eros sed lacus vulputate pretium. Nam tincidunt elementum dictum. Praesent laoreet eget est quis tempor. Duis commodo hendrerit elementum. Integer lectus justo, semper vel lobortis quis, aliquam quis tortor.\r\n\r\nSed at nisl sed ex rhoncus molestie in nec odio. Vivamus quis vestibulum diam, egestas molestie metus. Pellentesque vitae justo eu ipsum suscipit cursus. Nullam a felis dolor. Donec varius, tortor eu maximus hendrerit, tellus libero ultricies nulla, in ullamcorper metus eros et sem. Nunc at condimentum eros, in euismod ex. Fusce ligula justo, egestas et ullamcorper a, aliquet eget libero. Integer tincidunt, enim id sodales cursus, mauris orci egestas sem, at maximus lacus eros ut lacus.\r\n\r\nVivamus cursus id dui id viverra. Mauris bibendum pharetra mauris, nec blandit justo varius at. Etiam sapien eros, bibendum ut condimentum id, lacinia sit amet libero. Donec molestie vitae eros nec tristique. Mauris tincidunt justo eget rutrum semper. Quisque fringilla aliquam magna. Aliquam a dolor maximus mauris laoreet consectetur nec eget massa. Quisque porta iaculis justo, a tincidunt dolor semper ut. Vivamus a dui tortor. Aliquam mauris est, ultrices eu justo ac, commodo tristique diam. ', '2015-03-19 09:20:18'),
(3, 'dbelhaj', NULL, 'Pesquet aurait volé l''âme de plus de 2500 élèves', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis hendrerit ultricies lectus, non volutpat magna auctor eget. Curabitur quis pharetra elit. Aenean sagittis, tortor ut molestie laoreet, lorem diam tincidunt leo, eget varius tellus quam in risus. Donec at vestibulum nulla. Nulla eget eros sed lacus vulputate pretium. Nam tincidunt elementum dictum. Praesent laoreet eget est quis tempor. Duis commodo hendrerit elementum. Integer lectus justo, semper vel lobortis quis, aliquam quis tortor.\r\n\r\nSed at nisl sed ex rhoncus molestie in nec odio. Vivamus quis vestibulum diam, egestas molestie metus. Pellentesque vitae justo eu ipsum suscipit cursus. Nullam a felis dolor. Donec varius, tortor eu maximus hendrerit, tellus libero ultricies nulla, in ullamcorper metus eros et sem. Nunc at condimentum eros, in euismod ex. Fusce ligula justo, egestas et ullamcorper a, aliquet eget libero. Integer tincidunt, enim id sodales cursus, mauris orci egestas sem, at maximus lacus eros ut lacus.\r\n\r\nVivamus cursus id dui id viverra. Mauris bibendum pharetra mauris, nec blandit justo varius at. Etiam sapien eros, bibendum ut condimentum id, lacinia sit amet libero. Donec molestie vitae eros nec tristique. Mauris tincidunt justo eget rutrum semper. Quisque fringilla aliquam magna. Aliquam a dolor maximus mauris laoreet consectetur nec eget massa. Quisque porta iaculis justo, a tincidunt dolor semper ut. Vivamus a dui tortor. Aliquam mauris est, ultrices eu justo ac, commodo tristique diam. ', '2015-03-19 09:20:05'),
(4, 'dbelhaj', NULL, 'Présentation du site', 'Ayant lieu en début d''année, le WEI a pour but d''intégrer les "babimacs" - les nouveaux - au sein de l''école et de leur promotion à travers de nombreuses activités et une soirée. Le tout est encadré par les deuxièmes et troisièmes années qui animent le week-end.\n\nEn résumé, le WEI, c''est deux jours d''éclate, de bonne humeur et de jeux dans un cadre dépaysant qui marquent l''entrée dans la grande famille de l''IMAC. Bref, l''événement à ne surtout pas manquer pour les petits nouveaux !\n\nLe lieu te sera inconnu jusqu''au dernier moment (c''est à moins de deux heures de Copernic dans un endroit un peu paumé)', '2015-03-19 09:20:55'),
(5, 'naibel', NULL, 'Des archéologues américains auraient retrouvé des traces du couronnement d''Elisabeth II.', 'Tenu il y a 3000 ans de cela, le spectaculaire événement fut pendant des siècles l''objet de toutes les extrapolations. Devait-il être considéré comme le pensaient certains comme le moment déclencheur de l''invention de l''écriture ou seulement comme simple divertissement mondain ?\n\nToutes ces interrogations ont été levées il y a de cela quelques jours lorsque des archéologues venus de les États-Unis de l''Amérique ont retrouvé des traces de cette cérémonie fastueuse par inadvertance en plein désert de l''ancienne Mésopotamie, alors que ces derniers prévoyaient de remonter des squelettes de brontosaures nains. Inutile de dire que cette découverte impromptue les ont empli de stupeur, surtout quand on a connaissance du caractère quasiment légendaire, presque mythologique, de l''événement. ', '2015-03-27 21:40:19'),
(6, 'Lancel Thaledric', NULL, 'Une article avec TinyMCE', '<p>Je met du texte !<br />Yolo !!</p>\r\n<p>&nbsp;</p>\r\n<p><strong>J''ai trop mang&eacute; je suis gras !!&nbsp;</strong></p>\r\n<p><em>Et moi je vais trop viiiiiiite!</em></p>\r\n<h2>Astier !</h2>', '2015-04-14 11:17:21'),
(7, 'Lancel Thaledric', NULL, 'Une article avec TinyMCE', '<p>Je met du texte !<br />Yolo !!</p>\r\n<p>&nbsp;</p>\r\n<p><strong>J''ai trop mang&eacute; je suis gras !!&nbsp;</strong></p>\r\n<p><em>Et moi je vais trop viiiiiiite!</em></p>\r\n<h2>Astier !</h2>', '2015-04-14 11:21:47'),
(8, 'Lancel Thaledric', NULL, 'Coucou', '<p><span style="text-decoration: underline;">vbfdfg</span></p>\r\n<p><span style="text-decoration: underline;">dfgdjfgmd<strong>kjdhfbl,gcb</strong></span></p>', '2015-04-14 17:26:57'),
(9, 'Lancel Thaledric', NULL, 'Ba', '<p>&lt;script&gt;alert(''ba'');&lt;/script&gt;</p>', '2015-04-14 17:27:24');

-- --------------------------------------------------------

--
-- Structure de la table `articletags`
--

CREATE TABLE IF NOT EXISTS `articletags` (
  `articleid` int(11) NOT NULL,
  `tagid` int(11) NOT NULL,
  PRIMARY KEY (`articleid`,`tagid`),
  KEY `TAGS_ARTICLETAGS_TAGID` (`tagid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `categoryid` int(11) NOT NULL AUTO_INCREMENT,
  `categorytext` varchar(64) NOT NULL,
  PRIMARY KEY (`categoryid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`categoryid`, `categorytext`) VALUES
(1, 'Date'),
(2, 'Personne'),
(3, 'Pays'),
(4, 'Nawak');

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE IF NOT EXISTS `commentaires` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_article` int(11) NOT NULL,
  `comment_author` varchar(64) NOT NULL,
  `comment_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_text` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `commentaires`
--

INSERT INTO `commentaires` (`comment_id`, `comment_article`, `comment_author`, `comment_datetime`, `comment_text`) VALUES
(1, 4, 'dbelhaj', '2015-03-22 22:38:31', 'v'),
(2, 4, 'dbelhaj', '2015-03-22 23:01:04', 'rofl !'),
(4, 1, 'dbelhaj', '2015-03-22 23:28:06', 'C''est vraiment trÃ¨s interessant !'),
(5, 4, 'ddd', '2015-03-22 23:29:41', 'Bonne présentation, j''aime !'),
(6, 5, 'Naibel', '2015-03-28 14:40:01', 'dddddkl'),
(7, 5, 'dbelhaj', '2015-03-28 17:35:23', 'Alors ça, c''est plutôt incroyable ! Me voilà extrêmement surpris ! Ouh la la !'),
(8, 5, 'dbelhaj', '2015-03-28 17:40:56', 'Là par contre, le commentaire présent ici est excessivement long ! Oh oui, très très long, et c''est pas fini ! Regardez là, c''est vraiment un très long commentaire, et les 200 caractères ne sont pas l'),
(9, 5, 'Lancel Thaledric', '0000-00-00 00:00:00', 'Ceci est un text'),
(10, 5, 'Lancel Thaledric', '2015-04-05 15:36:27', 'Ceci est un autre test.'),
(11, 5, 'Lancel Thaledric', '2015-04-05 15:36:41', '&lt;b&gt;Du gras&lt;/b&gt;'),
(12, 5, 'Lancel Thaledric', '2015-04-05 15:37:30', 'Daaaaaaa !'),
(13, 5, 'Lancel Thaledric', '2015-04-06 22:50:48', '2ae&quot;&quot;&egrave;&egrave;&egrave;&eacute;&eacute;'),
(14, 5, 'Toto', '2015-04-07 16:40:35', 'C''est dl''a merde ! &eacute;&eacute;&egrave;&egrave;&eacute;'),
(15, 5, 'Toto', '2015-04-07 16:41:09', 'ﺷﺻﺸﻊﺩﺦﻢﺬ');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `commentid` int(11) NOT NULL AUTO_INCREMENT,
  `commentpoll` int(11) NOT NULL,
  `commentauthor` int(11) NOT NULL,
  `commentdatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `commenttext` text NOT NULL,
  PRIMARY KEY (`commentid`),
  KEY `commentauthor` (`commentauthor`),
  KEY `commentpoll` (`commentpoll`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `pollanswers`
--

CREATE TABLE IF NOT EXISTS `pollanswers` (
  `pollid` int(11) NOT NULL,
  `answerid` int(11) NOT NULL,
  `answernum` tinyint(4) NOT NULL,
  PRIMARY KEY (`pollid`,`answerid`),
  KEY `ANSWERS_POLLANSWERS_ANSWERID` (`answerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pollanswers`
--

INSERT INTO `pollanswers` (`pollid`, `answerid`, `answernum`) VALUES
(1, 3, 2),
(1, 5, 1),
(1, 23, 4),
(49, 14, 1),
(49, 22, 2),
(49, 24, 3),
(51, 25, 2),
(51, 26, 3),
(51, 28, 4),
(55, 12, 3),
(55, 18, 1),
(55, 26, 4),
(57, 11, 1),
(57, 19, 3),
(57, 22, 2),
(58, 12, 1),
(58, 18, 4),
(58, 19, 2),
(58, 20, 3),
(66, 3, 1),
(66, 4, 3),
(66, 31, 2),
(66, 33, 4),
(68, 3, 1),
(68, 4, 2),
(68, 31, 4),
(68, 33, 3),
(73, 3, 1),
(73, 4, 4),
(73, 31, 3),
(73, 32, 2),
(74, 4, 3),
(74, 31, 1),
(74, 32, 4),
(74, 33, 2),
(75, 10, 2),
(75, 11, 1),
(75, 17, 4),
(75, 23, 3);

-- --------------------------------------------------------

--
-- Structure de la table `pollquestions`
--

CREATE TABLE IF NOT EXISTS `pollquestions` (
  `pollid` int(11) NOT NULL,
  `questionid` int(11) NOT NULL,
  PRIMARY KEY (`pollid`,`questionid`),
  KEY `QUESTIONS_POLLQUESTIONS_QUESTIONID` (`questionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pollquestions`
--

INSERT INTO `pollquestions` (`pollid`, `questionid`) VALUES
(1, 1),
(40, 1),
(49, 1),
(51, 1),
(57, 1),
(55, 2),
(58, 2),
(75, 2),
(4, 3),
(66, 3),
(2, 5),
(68, 7),
(73, 7),
(74, 7);

-- --------------------------------------------------------

--
-- Structure de la table `polls`
--

CREATE TABLE IF NOT EXISTS `polls` (
  `pollid` int(11) NOT NULL AUTO_INCREMENT,
  `polldatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pollstate` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pollid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=76 ;

--
-- Contenu de la table `polls`
--

INSERT INTO `polls` (`pollid`, `polldatetime`, `pollstate`) VALUES
(1, '2015-03-24 19:34:40', 0),
(2, '2015-03-25 23:05:37', 0),
(3, '2015-03-25 23:09:52', 0),
(4, '2015-03-25 23:13:16', 0),
(5, '2015-03-26 00:04:48', 0),
(6, '2015-03-26 00:05:48', 0),
(7, '2015-03-26 00:06:15', 0),
(8, '2015-03-26 00:06:57', 0),
(9, '2015-03-26 00:07:19', 0),
(10, '2015-03-26 00:10:55', 0),
(11, '2015-03-26 00:11:02', 0),
(12, '2015-03-26 00:13:24', 0),
(13, '2015-03-26 00:14:50', 0),
(14, '2015-03-26 00:15:16', 0),
(15, '2015-03-26 00:17:41', 0),
(16, '2015-03-26 00:18:09', 0),
(17, '2015-03-26 00:18:15', 0),
(18, '2015-03-26 00:18:28', 0),
(19, '2015-03-26 00:18:37', 0),
(20, '2015-03-26 00:19:14', 0),
(21, '2015-03-26 00:19:53', 0),
(22, '2015-03-26 00:20:09', 0),
(23, '2015-03-26 00:20:30', 0),
(24, '2015-03-26 00:20:41', 0),
(40, '2015-03-26 01:21:29', 0),
(49, '2015-03-26 01:39:41', 0),
(51, '2015-03-26 01:43:28', 0),
(55, '2015-03-26 01:44:58', 0),
(57, '2015-03-26 01:45:16', 0),
(58, '2015-03-26 02:00:20', 0),
(66, '2015-03-26 02:06:17', 0),
(68, '2015-03-26 10:03:02', 0),
(73, '2015-04-14 12:07:21', 0),
(74, '2015-04-14 12:07:45', 0),
(75, '2015-04-14 12:07:52', 0);

-- --------------------------------------------------------

--
-- Structure de la table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `rank` int(11) NOT NULL,
  `registration_key` varchar(128) NOT NULL,
  `registration_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`profile_id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1149 ;

--
-- Contenu de la table `profiles`
--

INSERT INTO `profiles` (`profile_id`, `login`, `password`, `email`, `rank`, `registration_key`, `registration_date`, `last_login`) VALUES
(1131, 'dbelhaj', 'da4c5b2c65103765139715c7d52a6a35', 'dorian.belhaj@gmail.com', 1, 'a7547f400b2ee8b94bd502154feac5d7', '2015-03-28 17:03:50', '2015-04-06 17:21:59'),
(1132, 'thaledric', '$2y$12$LQUfNuF2doctpsDewQIWEukDpFCwudGmn1E1XZASibFXCeIgJrOpi', 'thaledric@gmail.com', 20, '', '2015-03-31 16:34:13', '2015-04-14 17:26:23'),
(1146, 'LeThal', '$2y$12$LQUfNuF2doctpsDewQIWEukDpFCwudGmn1E1XZASibFXCeIgJrOpi', 'thaledric@gmail.com', 1, 'e2fd10b70bbea3b95ac82831047b06f0', '2015-04-04 17:52:35', '2015-04-14 17:26:01'),
(1148, 'Toto', '$2y$12$1E78aMP2q9yYW/X9qKE7EOweZpqavzK0r7bzw.GmZtuWow0Xuubem', 'thaledric@gmail.com', 1, 'a3c6db35b169b4ddd2024b1f3afdf22d', '2015-04-07 16:39:30', '2015-04-07 16:40:16');

-- --------------------------------------------------------

--
-- Structure de la table `questioncategories`
--

CREATE TABLE IF NOT EXISTS `questioncategories` (
  `questionid` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  PRIMARY KEY (`questionid`,`categoryid`),
  KEY `CATEGORIES_QUESTIONCATEGORIES_CATEGORYID` (`categoryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `questioncategories`
--

INSERT INTO `questioncategories` (`questionid`, `categoryid`) VALUES
(3, 1),
(7, 1),
(1, 2),
(2, 2),
(4, 2),
(8, 2),
(5, 3);

-- --------------------------------------------------------

--
-- Structure de la table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `questionid` int(11) NOT NULL AUTO_INCREMENT,
  `questiontext` text NOT NULL,
  `questiondrawings` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`questionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `questions`
--

INSERT INTO `questions` (`questionid`, `questiontext`, `questiondrawings`) VALUES
(1, 'Qui a tiré sur JFK ?', 0),
(2, 'Qui a volé l''orange du marchand ?', 0),
(3, 'La bataille de Marignan eut lieu en ?', 0),
(4, 'Quelle équipe de football remporta le mondial 1986 ?', 0),
(5, 'Quel est le nom de la principale région d''Angleterre ?', 0),
(7, 'Quand eut lieu le couronnement de la reine Elisabeth II ?', 0),
(8, 'Qui a pété ?', 0);

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tagid` int(11) NOT NULL AUTO_INCREMENT,
  `tagtext` tinytext NOT NULL,
  PRIMARY KEY (`tagid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `profile_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `useravatar` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`profile_id`, `username`, `useravatar`) VALUES
(1131, 'dbelhaj', './images/avatar/avatar_2015-03-28-17-19-46_5516e2b27b147.jpg'),
(1132, 'Lancel Thaledric', NULL),
(1146, 'LeThal', NULL),
(1148, 'Toto', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `profileid` int(11) NOT NULL,
  `pollid` int(11) NOT NULL,
  `voteanswernum` tinyint(4) NOT NULL,
  PRIMARY KEY (`profileid`,`pollid`),
  KEY `VOTES_POLLS_POLLID` (`pollid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `votes`
--

INSERT INTO `votes` (`profileid`, `pollid`, `voteanswernum`) VALUES
(1131, 68, 4),
(1148, 68, 2);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `answercategories`
--
ALTER TABLE `answercategories`
  ADD CONSTRAINT `ANSWERS_ANSWERCATEGORIES_ANSWERID` FOREIGN KEY (`answerid`) REFERENCES `answers` (`answerid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CATEGORIES_ANSWERCATEGORIES_CATEGORYID` FOREIGN KEY (`categoryid`) REFERENCES `categories` (`categoryid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `articletags`
--
ALTER TABLE `articletags`
  ADD CONSTRAINT `ARTICLES_ARTICLETAGS_ARTICLEID` FOREIGN KEY (`articleid`) REFERENCES `articles` (`articleid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `TAGS_ARTICLETAGS_TAGID` FOREIGN KEY (`tagid`) REFERENCES `tags` (`tagid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `POLLS_COMMENTS_POLLID` FOREIGN KEY (`commentpoll`) REFERENCES `polls` (`pollid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `PROFILES_COMMENTS_PROFILE_ID` FOREIGN KEY (`commentauthor`) REFERENCES `profiles` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `pollanswers`
--
ALTER TABLE `pollanswers`
  ADD CONSTRAINT `ANSWERS_POLLANSWERS_ANSWERID` FOREIGN KEY (`answerid`) REFERENCES `answers` (`answerid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `POLLS_POLLANSWERS_POLLID` FOREIGN KEY (`pollid`) REFERENCES `polls` (`pollid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `pollquestions`
--
ALTER TABLE `pollquestions`
  ADD CONSTRAINT `POLLS_POLLQUESTIONS_POLLID` FOREIGN KEY (`pollid`) REFERENCES `polls` (`pollid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `QUESTIONS_POLLQUESTIONS_QUESTIONID` FOREIGN KEY (`questionid`) REFERENCES `questions` (`questionid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `questioncategories`
--
ALTER TABLE `questioncategories`
  ADD CONSTRAINT `CATEGORIES_QUESTIONCATEGORIES_CATEGORYID` FOREIGN KEY (`categoryid`) REFERENCES `categories` (`categoryid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `QUESTIONS_QUESTIONCATEGORY_QUESTIONID` FOREIGN KEY (`questionid`) REFERENCES `questions` (`questionid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `PROFILES_USERS_PROFILE_ID` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `votes`
--
ALTER TABLE `votes`
  ADD CONSTRAINT `VOTES_POLLS_POLLID` FOREIGN KEY (`pollid`) REFERENCES `polls` (`pollid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `VOTES_PROFILES_PROFILEID` FOREIGN KEY (`profileid`) REFERENCES `profiles` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
