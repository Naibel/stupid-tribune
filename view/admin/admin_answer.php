<?php if(User::isLogged()){ ?>
	<span class="sous_titre" style="font-size:24px;">Hey, <?php $this->pv('user_username'); ?> ! Si t'as une réponse stupide à soumettre, vas-y !</span><br/><br/>
		
	<form action="<?php $this->pv('current_url'); ?>" method="post">

		<span class="sous_titre_2">Réponse à ajouter :</span>
		<input class="champ_blanc" type="text" name="answer_text" required></br></br></br></br>

		<span class="sous_titre_2">Catégorie de la réponse:</span>
		<select class="champ_blanc" name="answer_category">
			<?php
				$list_categ=Admin::listCateg();

				foreach ($list_categ as $key => $value) {
			?>
					<option value="<?php echo $list_categ[$key]['categorytext']; ?>"><?php echo $list_categ[$key]['categorytext']; ?></option> 
			<?php
				}
			?>
		</select>

		<input class="button" type="submit" name="answersend" value="Envoyer"><br/><br/>

	</form>

	<span style="font-size:18px; color:#550000;"><a href="<?php $this->pv('home_url'); ?>">Revenir à l'accueil du back-office</a></span>
<?php } else { ?>
    <span class="sous_titre_2">Vous devez être connecté pour acceder à cette partie du site.</span></br></br>
<?php } ?>