<?php

class Admin extends Model
{
    public static $articles = 'articles';
    public static $answers = 'answers';
    public static $questions = 'questions';
    public static $answercategories = 'answercategories';
    public static $questioncategories = 'questioncategories';
    public static $categories = 'categories';
    public static $polls = 'polls';
    public static $pollanswers = 'pollanswers';
    public static $pollquestions = 'pollquestions';
    
    public function __construct($content = null)
    {
        parent::__construct();
    }
    
    /**
     * @brief Retourne tous les commentaire de l'article $id.
     * @param in int $id L'id de l'article duquel récupérer les commentaires.
     * @return Un tableau d'objets Comment.
     */

    public static function addQuestion($question, $category)
    {
        try{
            Model::beginTransaction();
            
            $sql = 'INSERT INTO `'.self::$questions.'`
                    SET questiontext = :question';

            $req = Db::get()->prepare($sql);
            $req->bindValue('question', $question, PDO::PARAM_STR);
            $req->execute();
            
            $sql = 'INSERT INTO `'.self::$questioncategories.'`
                    SET questionid = (SELECT questionid FROM `'.self::$questions.'` WHERE questiontext = :question), 
                        categoryid = (SELECT categoryid FROM `'.self::$categories.'` WHERE categorytext = :category)';

            $req = Db::get()->prepare($sql);
            $req->bindValue('question', $question, PDO::PARAM_STR);
            $req->bindValue('category', $category, PDO::PARAM_STR);
            $req->execute();

            Model::commitTransaction();

            MsgQueue::addMessage('Et hop ! Une question envoyée !');
            
            return true;
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }

    public static function listQuestions()
    {
        try{

            Model::beginTransaction();
            
            $sql = 'SELECT * FROM `'.self::$questions.'`';

            $req = Db::get()->prepare($sql);
            $req->execute();

            $req->setFetchMode(PDO::FETCH_ASSOC);
            $list = $req->fetchAll();

            Model::commitTransaction();
            
            return $list;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }


    public static function listCateg()
    {
        try{

            Model::beginTransaction();
            
            $sql = 'SELECT * FROM `'.self::$categories.'`';

            $req = Db::get()->prepare($sql);
            $req->execute();

            $req->setFetchMode(PDO::FETCH_ASSOC);
            $list = $req->fetchAll();

            Model::commitTransaction();
            
            return $list;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public static function addAnswer($answer, $category)
    {
        try{
            Model::beginTransaction();
            
            $sql = 'INSERT INTO `'.self::$answers.'`
                    SET answertext = :answer';

            $req = Db::get()->prepare($sql);
            $req->bindValue('answer', $answer, PDO::PARAM_STR);
            $req->execute();
            
            $sql = 'INSERT INTO `'.self::$answercategories.'` 
            SET answerid = (SELECT answerid FROM `'.self::$answers.'` WHERE answertext = :answer), 
                categoryid = (SELECT categoryid FROM `'.self::$categories.'` WHERE categorytext = :category)';

            $req = Db::get()->prepare($sql);
            $req->bindValue('answer', $answer, PDO::PARAM_STR);
            $req->bindValue('category', $category, PDO::PARAM_STR);
            $req->execute();

            Model::commitTransaction();

            MsgQueue::addMessage('Et hop ! Une réponse envoyée !');
            
            return true;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }

    public static function addArticle($login, $title, $content)
    {
        try{

            Model::beginTransaction();
            
            $sql = 'INSERT INTO `'.self::$articles.'`
                    (`articleauthor`,
                     `articletitle`,
                     `articletext`,
                     `articledatetime`)
                    VALUES
                    (:login, :titre, :content, NOW())';
            $req = Db::get()->prepare($sql);
            $req->bindValue('login', $login, PDO::PARAM_STR);
            $req->bindValue('titre', $title, PDO::PARAM_STR);
            $req->bindValue('content', $content, PDO::PARAM_STR);
            $req->execute();

            Model::commitTransaction();
            
            MsgQueue::addMessage('Et hop ! Un article envoyé !');

            return true;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public static function addPoll($text_question)
    {
        try{
            Model::beginTransaction();
            
            // Etape 1 : On récupère la question
            
            $sql = 'SELECT * FROM `'.self::$questions.'` WHERE questiontext = :text_question';
            $req = Db::get()->prepare($sql);
            $req->bindValue('text_question', $text_question, PDO::PARAM_STR);
            $req->execute();

            $req->setFetchMode(PDO::FETCH_ASSOC);
            $quest = $req->fetch();
            
            $id_question=$quest['questionid'];

            // Etape 2 : On récupère la catégorie de la question

            $sql = 'SELECT categoryid FROM `'.self::$questioncategories.'` WHERE questionid = :id_question';
            $req = Db::get()->prepare($sql);
            $req->bindValue('id_question', $id_question, PDO::PARAM_STR);
            $req->execute();

            $req->setFetchMode(PDO::FETCH_ASSOC);
            $categ = $req->fetch();
            
            $id_category=$categ['categoryid'];
            
            //debugn('id_category', $id_category);

            // Etape 3 : On récupère 4 réponses aléatoires de la catégorie de la question

            $sql = 'SELECT * FROM `'.self::$answercategories.'` 
                    INNER JOIN answers ON `'.self::$answercategories.'`.answerid=`'.self::$answers.'`.answerid 
                    WHERE categoryid = :id_category
                    ORDER BY rand() 
                    LIMIT 4';

            $req = Db::get()->prepare($sql);
            $req->bindValue('id_category', $id_category, PDO::PARAM_STR);
            $req->execute();

            $req->setFetchMode(PDO::FETCH_ASSOC);
            $rep = $req->fetchAll();
            //debugn('rep answer', $rep);
            if(!isset($rep[3]))
            {
                MsgQueue::addMessage('Erreur : pas assez de réponses pour la catégorie de la question.');
                return false;
            }

            // INSERER SONDAGE DANS LA TABLE "polls"

            $sql = 'INSERT INTO `'.self::$polls.'` (`polldatetime`, `pollstate`) VALUES (NOW(), 0)';

            $req = Db::get()->prepare($sql);
            $req->execute();
            
            $sql = 'SELECT MAX(`pollid`) AS `nbr` FROM `'.self::$polls.'`';
            $req = Db::get()->prepare($sql);
            $req->execute();
            $id_poll = $req->fetch(PDO::FETCH_NUM)[0];
            
            //INSERTION DE LA QUESTION ET DES REPONSES

            for($i=0;$i<=3;$i++){
                $sql = 'INSERT INTO `'.self::$pollanswers.'`
                        SET `answernum` = :i, 
                            `pollid` = (SELECT `pollid` FROM `'.self::$polls.'` WHERE `pollid` = :pollid), 
                            `answerid` = (SELECT answerid FROM `'.self::$answers.'` WHERE answertext = :text_answer)';

                $req = Db::get()->prepare($sql);
                $req->bindValue('i', $i+1, PDO::PARAM_INT);
                $req->bindValue(':pollid', $id_poll, PDO::PARAM_INT);
                $req->bindValue('text_answer', $rep[$i]['answertext'], PDO::PARAM_STR);
                $req->execute();
            }

            //INSERER SONDAGE DANS LA TABLE "pollquestions"

            $sql = 'INSERT INTO `'.self::$pollquestions.'`
                    SET `pollid` = (SELECT `pollid` FROM `'.self::$polls.'` WHERE `pollid` = :pollid), 
                    `questionid` = (SELECT `questionid` FROM `'.self::$questions.'` WHERE `questiontext` = :text_question);';

            $req = Db::get()->prepare($sql);
            $req->bindValue('text_question', $text_question, PDO::PARAM_STR);
            $req->bindValue(':pollid', $id_poll, PDO::PARAM_INT);
            $req->execute();

            Model::commitTransaction();

            MsgQueue::addMessage('Et hop ! Un sondage généré !');

            return true;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
}