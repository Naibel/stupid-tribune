<?php

class AdminController extends Controller
{
    
    public function defaultAction()
    {
        $this->home();
    }    
    
    public function home()
    {
        if(!$this->checkRights()) return;
        
        Model::loadModel('admin');
        Model::loadModel('user');

        $this->createView('admin_home');

        $this->view->set('admin_question_url', Router::url('admin/question'));
        $this->view->set('admin_answer_url', Router::url('admin/answer'));
        $this->view->set('admin_poll_url', Router::url('admin/poll'));
        $this->view->set('admin_article_url', Router::url('admin/article'));
        $this->view->set('home_url', Router::url(''));

        if(User::isLogged()){
            $this->view->set('user_username', User::getLogged()->username);
        }

        $this->setLayout('adminLayout');
    }

    public function article()
    {
        if(!$this->checkRights()) return;
        
        Model::loadModel('admin');
        Model::loadModel('user');

        $author=User::getLogged()->username;

        $this->createView('admin_article'); 

        $this->view->set('current_url', Router::url(Request::getClientUrl()));
        
        $this->view->set('user_username', $author);

        $this->view->set('home_url', Router::url('admin'));

        $this->setLayout('adminLayout');
    }

    public function articlesend()
    {
        if(!$this->checkRights()) return;
        
        Model::loadModel('admin');
        Model::loadModel('user');

        $author=User::getLogged()->username;

        $title = Dispatcher::inputPost('article_title', Dispatcher::PARAM_STR);
        $content = Dispatcher::inputPost('article_content', Dispatcher::PARAM_STR);
        
        Admin::addArticle($author, $title, $content);
    }

    
    public function answer()
    {
        if(!$this->checkRights()) return;
        
        Model::loadModel('admin');
        Model::loadModel('user');

        $this->createView('admin_answer');

        $this->view->set('current_url', Router::url(Request::getClientUrl()));
        
        $this->view->set('user_username', User::getLogged()->username);

        $this->view->set('home_url', Router::url('admin'));

        $this->setLayout('adminLayout');
    }

    public function answersend()
    {
        if(!$this->checkRights()) return;
        
        Model::loadModel('admin');
        Model::loadModel('user');

        $this->createView('admin_answer');
        
        $answer = Dispatcher::inputPost('answer_text', Dispatcher::PARAM_STR);
        $category = Dispatcher::inputPost('answer_category', Dispatcher::PARAM_STR);

        Admin::addAnswer($answer, $category);
    }
    
    public function question()
    {
        if(!$this->checkRights()) return;
        
        Model::loadModel('admin');
        Model::loadModel('user');

        $this->createView('admin_question');

        $this->view->set('current_url', Router::url(Request::getClientUrl()));

        $this->view->set('user_username', User::getLogged()->username);

        $this->view->set('home_url', Router::url('admin'));

        $this->setLayout('adminLayout');
    }

    public function questionsend()
    {
        if(!$this->checkRights()) return;
        
        Model::loadModel('admin');
        Model::loadModel('user');
        
        $question = Dispatcher::inputPost('question_text', Dispatcher::PARAM_STR);
        $category = Dispatcher::inputPost('question_category', Dispatcher::PARAM_STR);

        Admin::addQuestion($question, $category);
    }

    public function poll()
    {
        if(!$this->checkRights()) return;
        
        Model::loadModel('admin');
        Model::loadModel('user');

        $this->createView('admin_poll');

        $this->view->set('current_url', Router::url(Request::getClientUrl()));

        $this->view->set('user_username', User::getLogged()->username);
        
        $this->view->set('home_url', Router::url('admin'));

        $list_questions=Admin::listQuestions();

        $this->setLayout('adminLayout');
    }

    public function pollsend()
    {
        if(!$this->checkRights()) return;
        
        Model::loadModel('admin');
        Model::loadModel('user');
        
        $poll_question = Dispatcher::inputPost('poll_question', Dispatcher::PARAM_STR);

        Admin::addPoll($poll_question);
    }
    
    public function checkRights()
    {
        Model::loadModel('user');// 
        if(User::getLogged()->isAdmin()) return true;
        
        $this->createView('notadmin');
        
        $this->view->set('fuck_url', Router::url('design/backgrounds/Drfetus.png'));
        
        
        $this->setLayout('adminLayout');
        return false;
    }
    
}