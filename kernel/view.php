<?php

/**
 * @brief Une vue est un objet permettant de générer du code front-end.
 */
class View
{
    protected $vars = array();
    protected $subs = array();
    
    protected $file;
    protected $valid = false;
    protected $rendered = 0;
    
    function __construct($file)
    {
        if(!file_exists($file))
        {
            echo 'Ceci n\'est pas une vue.';
            return;
        }
        $this->file = $file;
        $this->valid = true;
    }
    
    function render()
    {
        if($this->valid)
        {
            ob_start();
            require($this->file);
            return ob_get_clean();
        }
    }
    
    public function set($key, $val=true)
    {
        if(is_array($key))
            $this->vars += $key;
        else
            $this->vars[$key] = $val;
    }
    
    public function subView($key, $ctrl)
    {
        $this->subs[$key] = $ctrl->getView();
    }
    
    public function pv($val)
    {
        echo $this->vars[$val];
    }
    
    public function v($val)
    {
        return $this->vars[$val];
    }
    
    public function iv($val)
    {
        return isset($this->vars[$val]);
    }
    
    public function ps($sub)
    {
        echo $this->subs[$sub];
    }
}