<?php

/**
 * @brief Le routeur parse l'url de l'utilisateur afin de créer une Request à traiter.
 */
class Router
{
    
    public static $routes = array(); //*< Tableau des routes par pattern
    public static $tasks = array();  //*< Tableau des tâches par nom
    
    /**
    *   Parse l'url passée en paramètre
    *   @param $url L'url à parser
    *   @return array contant les paramètres
    **/
    static function parse($req)
    {
        // Parse the path
        //$req->url = trim($req->url, '/'); // Done in Resquest Class
        
        // Check in routes to transform
        $req->url = self::deroute($req->url);
        //debugn('DEROUTED :', $req->url);
        
        // Construct the request
        if(!$req->url)
        {
            $req->params = array();
            return;
        }
        $req->params = explode('/', $req->url);
        $req->controller = $req->params[0];
        if($req->controller == 'index.php') $req->controller = null;
        $req->action = isset($req->params[1]) ? $req->params[1] : null;
        $req->params = array_slice($req->params, 2);
    }
    
    /**
     * @brief Crée une route.
     * @param in string $pat La route au format user-friendly.
     * @param in string $url L'url directement compréhensible par le dispatcher.
     * @param in array $elems Les élémens de l'url en clés et leur format PCRE en valeur
     * @return Description of returned value.
     */
    public static function connect($pat, $url, $elems)
    {
        $r = array();
        // Récupération du pattern
        $r['pattern'] = $pat;
        $r['patternR'] = self::regexize($pat, $elems);
        //debug($r['patternR']);
        
        // Récupération de la destination
        $r['url'] = $url;
        $r['urlR'] = self::regexize($url, $elems);
        //debug($r['urlR']);
        
        // Récupération des éléments
        $r['elems'] = $elems;
        
        // Insertion de la route (On utilise $pat comme index pour éviter les multis)
        self::$routes[$pat] = $r;
    }
    
    /**
     * @brief Génère la route à partir du pattern $url
     * @param in string $url L'url à convertir en route.
     * @param in bool $relative Ajoute le http://<racine> devant la route finale, pour pouvoir l'utiliser directement dans un lien.
     * @return La route formattée, prête-à-utiliser.
     */
    public static function url($url, $relative = false)
    {
        $ret;
        foreach(self::$routes as $r)
        {
            $count = 0;
            $ret = preg_replace_callback(
                $r['urlR'],
                function ($matches) use ($r)
                {
                    // Cette fonction recopie la valeur des éléments de $url dans le pattern correspondant
                    //debugn('matches : ', $matches);
                    
                    // Tous les noms des elems à placer...
                    $searches = array_keys($r['elems']);
                    array_walk($searches,
                        function(&$s, $k)
                        {
                            $s =  ':'.$s;
                        }
                    );
                    //debugn('Searches : ', $searches);
                    
                    // ... et leur valeur.
                    $replaces = $matches;
                    foreach($replaces as $k => $v)
                    {
                        if(is_numeric($k)) unset($replaces[$k]);
                    }
                    //debugn('Replaces : ', $replaces);
                    
                    // Si le callback est exécuté alors on a trouvé une route, donc pas besoin de tester les autres !
                    $found = true;
                    
                    return str_replace($searches, $replaces, $r['pattern']);
                },
                $url,
                -1,
                $count
            );
            //debugn('RET : ', $ret);
            if($count)
            {
                if(!$relative) $ret = BASE_URL . $ret;
                //debugn('RET : ', $ret);
                return $ret;
            }
        }
        
        if(!$relative) $url = BASE_URL . $url;
        //debugn('URL : ', $url);
        return $url;
    }
    
    /**
     * @brief Génère le chemin à partir de la route $route.
     * @param in string $route route à transformer.
     * @return le chemin au format controlleur/action/params
     */
    public static function deroute($route)
    {
        //debugn('INItial route', $route);
        foreach(self::$routes as $r)
        {
            //debugn('', $r);
            $count = 0;
            $ret = preg_replace_callback(
                $r['patternR'],
                function ($matches) use ($r)
                {
                    // Cette fonction recopie la valeur des éléments de $url dans le pattern correspondant
                    //debugn('matches : ', $matches);
                    
                    // Tous les noms des elems à placer...
                    $searches = array_keys($r['elems']);
                    array_walk($searches,
                        function(&$s, $k)
                        {
                            $s =  ':'.$s;
                        }
                    );
                    //debugn('Searches : ', $searches);
                    
                    // ... et leur valeur.
                    $replaces = $matches;
                    foreach($replaces as $k => $v)
                    {
                        if(is_numeric($k)) unset($replaces[$k]);
                    }
                    //debugn('Replaces : ', $replaces);
                    
                    // Si le callback est exécuté alors on a trouvé une route, donc pas besoin de tester les autres !
                    return str_replace($searches, $replaces, $r['url']);
                },
                $route,
                -1,
                $count
            );
            //debugn('RET : ', $ret);
            if($count) return $ret;
            
        }
        return $ret;
    }
    
    /**
     * @brief Transforme la route $pat en regex en utilisant les valeurs du tableau $elems
     * @param in string $pat La route à transformer
     * @param in array $elems Les éléments de la route à placer, de la forme element => format PCRE
     * @return retourne la regex générée.
     */
    public static function regexize($pat, $elems)
    {
        return  '#^' . 
            preg_replace_callback(
                '#:([a-z]+)#', 
                function ($matches) use ($elems)
                {
                    if(isset($elems[$matches[1]]))
                        return '(?P<' . $matches[1] . '>' . $elems[$matches[1]] . ')';
                    else
                        return $matches[0];
                },
                $pat
            )
            . '$#';
    }
    
    public static function addTask($name, $path)
    {
        self::$tasks[$name] = $path;
    }
    
    public static function getTask($name)
    {
        return isset(self::$tasks[$name]) ? self::$tasks[$name] : false;
    }
    
}
