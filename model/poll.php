<?php

//Dependencies
Model::loadModel('answer');

class Poll extends Model
{
    public static $table = 'polls';
    public static $questionstable = 'questions';
    public static $pollquestionstable = 'pollquestions';
    public static $pollanswerstable = 'pollanswers';
    public static $votestable = 'votes';
    
    public $pollid;
    public $polldatetime;
    public $pollstate;
    public $questionid;
    public $questiontext;
    public $answers;
    
    public function __construct($content = null)
    {
        parent::__construct();
    }
    
    /**
     * @brief Retourne un objet Patate récupéré depuis la BDD par son id.
     * @param in int $id L'id de la patate à récupérer.
     * @return retourne la Patate récupérée, null sinon.
     */
    public static function getById($id, $content = true)
    {
        try{
            Model::beginTransaction();
            
            // Etape 1 : on récupère le dernier sondage (son id surtout).
            
            $sql = 'SELECT * 
                    FROM `'.self::$table.'`
                    WHERE `pollid` = :id';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':id', $id, PDO::PARAM_INT);
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Poll', array());
            $req->execute();
            $ret = $req->fetch();

            // Si n'a pas demandé de $content, on ne poursuit pas.
            if(!$content) return $ret;
                
            $ret->getQuestion();
            $ret->answers = Answer::getByPoll($ret->pollid);
            
            Model::commitTransaction();
            
            return $ret;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public static function getLast($content = true)
    {
        try{
            Model::beginTransaction();
            
            // Etape 1 : on récupère le dernier sondage (son id surtout).
            
            $sql = 'SELECT * 
                    FROM `'.self::$table.'`
                    WHERE `pollid` = ( SELECT MAX(`pollid`) FROM `'.self::$table.'` )';
            $req = Db::get()->prepare($sql);
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Poll', array());
            $req->execute();
            $ret = $req->fetch();

            // Si n'a pas demandé de $content, on ne poursuit pas.
            if(!$content) return $ret;
                
            $ret->getQuestion();
            $ret->answers = Answer::getByPoll($ret->pollid);
            
            // Etape 2 :
            
            Model::commitTransaction();
            
            return $ret;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public function getQuestion()
    {
        try{
            Model::beginTransaction();
            
            $sql = 'SELECT * FROM `'.self::$pollquestionstable.'`
                        LEFT JOIN `'.self::$questionstable.'`
                        ON `'.self::$pollquestionstable.'`.`questionid`
                            = `'.self::$questionstable.'`.`questionid`
                    WHERE `'.self::$pollquestionstable.'`.`pollid` = :id';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':id', $this->pollid, PDO::PARAM_INT);
            $req->setFetchMode(PDO::FETCH_INTO, $this);
            $req->execute();
            $req->fetch();
            
            Model::commitTransaction();
            
            return true;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    
    public static function getVote($poll, $user, $content = false)
    {
        $idpoll = $poll->pollid;
        $iduser = $user->profile_id;
        
        try{
            Model::beginTransaction();
            $sql;
            if(!$content)
            {
                $sql = 'SELECT `voteanswernum` AS `answernum`
                        FROM `'.self::$votestable.'`
                        WHERE `profileid` = :profileid
                        AND `pollid` = :pollid';
                //var_dump($sql);
            }
            else
            {
                $sql = 'SELECT `voteanswernum` AS `answernum`,
                                `answers`.`answertext`,
                                `answers`.`answerid`
                        FROM `'.self::$votestable.'`
                        LEFT JOIN `'.self::$pollanswerstable.'`
                            ON `'.self::$votestable.'`.`voteanswernum` = `'.self::$pollanswerstable.'`.`answernum` 
                            AND `'.self::$votestable.'`.`pollid` = `'.self::$pollanswerstable.'`.`pollid`
                        LEFT JOIN `'.Answer::$table.'`
                            ON `'.Answer::$table.'`.`answerid` = `'.self::$pollanswerstable.'`.`answerid`
                        WHERE `'.self::$votestable.'`.`profileid` = :profileid
                        AND `'.self::$votestable.'`.`pollid` = :pollid';
                
                //var_dump($sql);
            }
            $req = Db::get()->prepare($sql);
            $req->bindValue(':profileid', $iduser, PDO::PARAM_INT);
            $req->bindValue(':pollid', $idpoll, PDO::PARAM_INT);
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Answer', array());
            $req->execute();
            $ret = $req->fetch();
            
            Model::commitTransaction();
            
            return empty($ret)? false : $ret;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public static function getLastPolls($num)
    {
        try{
            Model::beginTransaction();
        
            $sql = 'SELECT  `'.self::$table.'`.`pollid`,
                            `'.self::$table.'`.`polldatetime`,
                            `'.self::$table.'`.`pollstate`,
                            `'.self::$pollquestionstable.'`.`questionid`,
                            `'.self::$questionstable.'`.`questiontext`
                    FROM `'.self::$table.'`
                        INNER JOIN `'.self::$pollquestionstable.'`
                            ON `'.self::$pollquestionstable.'`.`pollid` = `'.self::$table.'`.`pollid`
                        INNER JOIN `'.self::$questionstable.'`
                            ON `'.self::$pollquestionstable.'`.`questionid` = `'.self::$questionstable.'`.`questionid`
                    ORDER BY `'.self::$table.'`.`pollid` DESC
                    LIMIT 0, :num';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':num', $num, PDO::PARAM_INT);
            $req->execute();
            
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Poll', array());
            $ret = $req->fetchAll();
            
            Model::commitTransaction();

            return $ret;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public static function exists($id)
    {
        if($id <= 0) return false;
        try{
            Model::beginTransaction();
            
            $sql = 'SELECT COUNT(`pollid`) AS `nbr`
                    FROM `'.self::$table.'`
                    WHERE `pollid` = :id';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':id', $id, PDO::PARAM_INT);
            $req->execute();
            $ret = $req->fetch(PDO::FETCH_NUM);
            
            Model::commitTransaction();
            
            return $ret[0];
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public static function vote($pollid, $answernum)
    {
        try{
            Model::beginTransaction();
            
            $sql = 'INSERT INTO `'.self::$votestable.'`
                    (`profileid`, `pollid`, `voteanswernum`)
                    VALUES
                    (:userid, :pollid, :votenum)';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':userid', User::getLogged()->profile_id, PDO::PARAM_INT);
            $req->bindValue(':pollid', $pollid, PDO::PARAM_INT);
            $req->bindValue(':votenum', $answernum, PDO::PARAM_INT);
            $req->execute();
            
            Model::commitTransaction();
            
            return true;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }

    public static function getResults($pollid){
        try{
            Model::beginTransaction();
            
            $sql = 'SELECT `voteanswernum`, COUNT(*) AS nbReponses FROM `'.self::$votestable.'` WHERE pollid=:pollid GROUP BY voteanswernum ';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':pollid', $pollid, PDO::PARAM_INT);
            $req->execute();
            $req->setFetchMode(PDO::FETCH_ASSOC);
            $list = $req->fetchAll();
            
            Model::commitTransaction();
            
            return $list;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }

    public static function getAllResults($pollid){
        try{
            Model::beginTransaction();
            
            $sql = 'SELECT COUNT(*) AS nbAllReponses FROM `'.self::$votestable.'` WHERE pollid=:pollid';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':pollid', $pollid, PDO::PARAM_INT);
            $req->execute();
            $req->setFetchMode(PDO::FETCH_ASSOC);
            $list = $req->fetchAll();
            
            Model::commitTransaction();
            
            return $list;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    
}