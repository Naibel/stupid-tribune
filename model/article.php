<?php

class Article extends Model
{
    public static $table = 'articles';
    
    public $articleid;
    public $articleauthor;
    public $articlepoll;
    public $articletitle;
    public $articletext;
    public $articledatetime;
    public $articleurl;
    
    public function __construct($content = null)
    {
        parent::__construct();
    }
    
    /**
     * @brief Retourne l'article ayat pour id $id.
     * @param in int $id L'id de l'article à récupérer.
     * @return Un objet de type Article.
     */
    public static function getById($id)
    {
        try{
            
            Model::beginTransaction();
        
            $obj = new Article();
            $sql = 'SELECT * FROM `'.self::$table.'`
                    WHERE `articleid` = :id';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':id', $id, PDO::PARAM_INT);
            $req->setFetchMode(PDO::FETCH_INTO, $obj);
            $req->execute();
            $req->fetch();
            
            Model::commitTransaction();
            
            $obj->genlink();

            return $obj;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
        
    }
    
    /**
     * @brief Retourne les derniers article en date. Il seront triés du plus récent au plus ancien.
     * @param in int $num Nombre d'articles à renvoyer.
     * @return Un tableau d'objets Article.
     */
    public static function getLastArticles($num)
    {
        try{
            Model::beginTransaction();
        
            $sql = 'SELECT * FROM `'.self::$table.'`
                    ORDER by `articledatetime` DESC
                    LIMIT 0, :num';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':num', $num, PDO::PARAM_INT);
            $req->execute();
            
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Article', array());
            $ret = $req->fetchAll();
            
            foreach($ret as $article)
            {
                $article->genlink();
            }
            
            Model::commitTransaction();

            return $ret;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public function genlink()
    {
        $this->articleurl = Router::url('article/show/'.$this->articleid);
    }
    
    public static function search($search)
    {
        try{
            Model::beginTransaction();
            
            $sql = 'SELECT * FROM `'.self::$table.'`
                    WHERE `articletitle` LIKE :search
                    ORDER BY `articleid`';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':search', '%'.$search.'%', PDO::PARAM_STR);
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Article', array());
            $req->execute();
            $ret = $req->fetchAll();
            
            foreach($ret as $article)
            {
                $article->genlink();
            }
            
            Model::commitTransaction();
            
            return $ret;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public static function exists($id)
    {
        if($id <= 0) return false;
        try{
            Model::beginTransaction();
            
            $sql = 'SELECT COUNT(`articleid`) AS `nbr`
                    FROM `'.self::$table.'`
                    WHERE `articleid` = :id';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':id', $id, PDO::PARAM_INT);
            $req->execute();
            $ret = $req->fetch(PDO::FETCH_NUM);
            
            Model::commitTransaction();
            
            return $ret[0];
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
}