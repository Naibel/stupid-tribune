<?php

// Load all components of kernel

require_once KERNEL.'functions.php';
require_once KERNEL.'clock.php';

require_once KERNEL.'router.php';
require_once KERNEL.'request.php';
require_once KERNEL.'dispatcher.php';
require_once KERNEL.'controller.php';
require_once KERNEL.'view.php';

require_once KERNEL.'dbmain.php';
require_once KERNEL.'model.php';

require_once ROOT.'routes.php';

// Init clock meter
$clock = null;
if(isdebug('time'))
{
    $clock = new Clock(true);
}

// LAUNCH THE APP !

session_start();
$dispatcher = new Dispatcher();

// END APP

// Display clock meter
if(isdebug('time'))
{
    $clock->display();
}
