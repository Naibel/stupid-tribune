<?php

// Dependencies
Model::loadModel('msgqueue');

class User extends Model
{
    private static $table = 'profiles';
    private static $votes = 'votes';
    private static $userstable = 'users';
    
    private static $sidx = 'user';  // Index of the SESSION
    
    const RANK_UNDEF = -5;
    const RANK_BANNED = -2;
    const RANK_UNVALID = -1;
    const RANK_VISITOR = 0;
    const RANK_MEMBER = 1;
    const RANK_WRITER = 5;
    const RANK_MODERATOR = 10;
    const RANK_ADMIN = 20;
    
    public $profile_id;
    public $login;
    public $email;
    public $password;
    public $rank;
    public $registration_key;
    public $registration_date;
    public $last_login;
    public $username;
    public $useravatar;
    public $nb_polls;
    
    public function __construct($content = null)
    {
        parent::__construct();
        
        $this->rank = self::RANK_UNDEF;
        
        if(is_array($content))
        {
            foreach($content as $key => $val)
            {
                $this->$key = $val;
            }
        }
    }
    
    /**
     * @brief Retourne un objet User récupéré depuis la BDD par son id.
     * @param in int $id L'id de l'utilisateur à récupérer.
     * @return retourne le User récupérée, null sinon.
     */
    public static function getById($id)
    {
        try{
            Model::beginTransaction();
            
            $obj = new User();
            $sql = 'SELECT * FROM `'.self::$table.'` INNER JOIN `'.self::$userstable.'` WHERE `profile_id` = :id';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':id', $id, PDO::PARAM_INT);
            $req->setFetchMode(PDO::FETCH_INTO, $obj);
            $req->execute();
            $req->fetch();
            
            Model::commitTransaction();
            
            return $obj;
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            User::logout();
            return false;
        }
    }

    public static function getNbPolls($id)
    {
        try{
            Model::beginTransaction();

            $sql = 'SELECT COUNT(*) AS nb_sondages FROM `'.self::$votes.'` WHERE profileid = :id';

            $req = Db::get()->prepare($sql);

            $req->bindValue(':id', $id, PDO::PARAM_STR);

            $req->execute();

            $req->setFetchMode(PDO::FETCH_ASSOC);
            $list = $req->fetchAll();

            $nb_polls=$list[0]['nb_sondages'];

            Model::commitTransaction();

            return $nb_polls;
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }

    public static function compress_image($source_url, $destination_url, $quality) {
        $info = getimagesize($source_url);
     
        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
     
        //save file
        imagejpeg($image, $destination_url, $quality);
     
        //return destination file
        return $destination_url;
    }

    public static function uploadAvatar($fichier, $login){
        try
            {   
                Model::beginTransaction();

                $sql = "UPDATE users SET useravatar='./web/design/backgrounds/avatar/$fichier' WHERE username = :login";
                
                $req = Db::get()->prepare($sql);

                $req->bindValue(':login', $login, PDO::PARAM_STR);

                $req->execute();

                //si jusque là tout se passe bien on valide la transaction
                Model::commitTransaction();

                MsgQueue::addMessage('Upload effectué avec succès !');

                return true;
            }

        catch(Exception $e)
            {
                Model::BDDError($e);
                return false;
            }
    }

    public static function getAvatar($id){
        try
            {   
                Model::beginTransaction();

                $sql = 'SELECT useravatar FROM `'.self::$userstable.'` WHERE `profile_id` = :id';
                
                $req = Db::get()->prepare($sql);

                $req->bindValue(':id', $id, PDO::PARAM_STR);

                $req->execute();

                $req->setFetchMode(PDO::FETCH_ASSOC);
                $list = $req->fetchAll();

                $avatar_url=$list[0]['useravatar'];

                Model::commitTransaction();

                return $avatar_url;
            }

        catch(Exception $e)
            {
                Model::BDDError($e);
                return false;
            }
    }
    
    
    /**
     * @brief Retourne vrai si l'utilisateur est loggué, faux sinon.
     */
    public static function isLogged()
    {
        return isset($_SESSION[self::$sidx]) and self::getLogged()->rank > self::RANK_VISITOR;
    }
    
    /**
     * @brief Retoune un objet User qui référence l'utilisateur connecté.
     */
    public static function getLogged()
    {
        return isset($_SESSION[self::$sidx]) ? unserialize($_SESSION[self::$sidx]) : new User(array('rank' => self::RANK_VISITOR));
    }
    
    /**
     * @brief Deconnecte l'utilisateur. Cette fonction suprimme la variable de session qui stocke l'utilisateur connecté.
     * @return Ne retourne rien.
     */
    public static function logout()
    {
        unset($_SESSION[self::$sidx]);
    }
    
    /**
     * @brief Loggue l'utilisateur à partir du couple login/mot de passe
     * @param in string $login Le login de l'utilisateur voulant se connecter. Cela peut-être son "login" ou son adresse e-mail.
     * @param in string $pass Le mot de passé hashé.
     * @return Retourne vrai ou faux selon si l'authentification a réussi ou pas.
     */
    public static function login($login, $pass)
    {
        try{
            if (User::isLogged()) User::logout();
            
            Model::beginTransaction();
            
            // Step 1 : le login/password/rank sont bons.
            $user = new User();
            $query = Db::get()->prepare('
                SELECT `profile_id`, `password`, `rank`
                FROM `'.self::$table.'`
                WHERE `login` = :login');
            $query->bindValue(':login', $login, PDO::PARAM_STR);
            $query->setFetchMode(PDO::FETCH_INTO, $user);
            $query->execute();
            $query->fetch();
            
            // Verif password
            if(!password_verify($pass, $user->password))
            {
                MsgQueue::addMessage('Login et/ou mot de passe incorrect(s).');
                return false;
            }
            // Verif rank
            if(!$user->isMember())
            {
                MsgQueue::addMessage('Votre compte n\'est pas encore validé. Regardez dans vos mails.');
                return false;
            }
            
            // Step 2 : On stocke l'utilisateur dans $_SESSION.
            
            $sql = 'SELECT `'.self::$table.'`.`profile_id`,
                           `'.self::$table.'`.`login`,
                           `'.self::$table.'`.`email`,
                           `'.self::$table.'`.`rank`,
                           `'.self::$table.'`.`registration_date`,
                           `'.self::$table.'`.`last_login`,
                           `'.self::$userstable.'`.`username`,
                           `'.self::$userstable.'`.`useravatar`
                FROM `'.self::$table.'`
                INNER JOIN `users`
                    ON `'.self::$table.'`.`profile_id` = `'.self::$userstable.'`.`profile_id`
                WHERE `'.self::$table.'`.`profile_id` = :id';
            $query = Db::get()->prepare($sql);
            $query->bindValue(':id', $user->profile_id, PDO::PARAM_INT);
            $query->setFetchMode(PDO::FETCH_INTO, $user);
            $query->execute();
            $query->fetch();
            
            // Step 3 : On indique dans la BDD la date de dernière connexion
            
            $sql = 'UPDATE `'.self::$table.'`
                SET `last_login` = NOW()
                WHERE `login` = :login';

            $query = Db::get()->prepare($sql);
            $query->bindValue(':login', $user->login, PDO::PARAM_STR);
            $query->execute();
            
            $sql = 'SELECT `last_login`
                FROM `'.self::$table.'`
                WHERE `login` = :login';
            $query = Db::get()->prepare($sql);
            $query->bindValue(':login', $user->login, PDO::PARAM_STR);
            $query->setFetchMode(PDO::FETCH_INTO, $user);
            $query->execute();
            $query->fetch();

            Model::commitTransaction();
            
            // On vient de charger l'user en mémoire il est frocément perpétué.
            $user->perpetuated = true;
            
            $_SESSION[self::$sidx] = serialize($user);
            
            if(isdebug('allMessages'))
                MsgQueue::addMessage('Login effectué avec succès.');
            
            return true;
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            User::logout();
            return false;
        }
        
    }
    
    /**
     * @brief Hashe le mot de passe passée en paramètre.
     * @param in string $pass Le mot de passe à hasher.
     * @return Le mot de passe hashé.
     */
    public static function hashpass($pass)
    {
        $options = [
            'cost' => 12,
        ];
        return password_hash($pass, PASSWORD_BCRYPT, $options);
    }
    
    public static function hashpasstester()
    {
        /**
         * Ce code va tester votre serveur pour déterminer quel serait le meilleur "cost".
         * Vous souhaitez définir le "cost" le plus élevé possible sans trop ralentir votre serveur.
         * 8-10 est une bonne base, mais une valeur plus élevée est aussi un bon choix à partir
         * du moment où votre serveur est suffisament rapide ! Le code suivant espère un temps
         * ≤ à 50 millisecondes, ce qui est une bonne base pour les systèmes gérants les identifications
         * intéractivement.
         */
        $timeTarget = 0.05; // 50 millisecondes

        $cost = 8;
        do {
            $cost++;
            $start = microtime(true);
            password_hash("test", PASSWORD_BCRYPT, ["cost" => $cost]);
            $end = microtime(true);
        } while (($end - $start) < $timeTarget);

        echo "Valeur de 'cost' la plus appropriée : " . $cost . "\n";
    }
    
    /**
     * @brief Renvoie vrai ou faux si l'utilisateur actuel est un membre ou pas.
     * @return Booléen
     */
    public function isMember()
    {
        return $this->rank > 0;
    }

    public function isWriter()
    {
        return $this->rank >= self::RANK_WRITER;
    }

    public function isModerator()
    {
        return $this->rank >= self::RANK_MODERATOR;
    }

    public function isAdmin()
    {
        return $this->rank >= self::RANK_ADMIN;
    }
    
    /**
     * @brief Enregistre un nouvel utilisateur
     * @param in string $login Le login du nouvel utilsateur.
     * @param in string $password Son mot de passe non-crypté.
     * @param in string $email L'adresse mail du nouvel utilisateur. Le format ne sera pas testé.
     * @return Vrai ou faux selon si l'inscription a réussi ou pas.
     */
    public static function register($login, $password, $email)
    {
        try{            
            Model::beginTransaction();
            
            // Step 1 : On vérifie que le le login n'existe pas déjà.
            $sql = 'SELECT COUNT(`profile_id`) AS `nbprofiles`
                FROM `'.self::$table.'`
                WHERE `login` = :login';     
            $query = Db::get()->prepare($sql);
            $query->bindValue(':login', $login, PDO::PARAM_STR);
            $query->execute();
            $res = $query->fetch(PDO::FETCH_ASSOC);
            
            if($res['nbprofiles'])
            {
                MsgQueue::addMessage('Ce pseudo existe déjà, veuillez en choisir un autre.');
                return false;
            }
            
            // Step 2 : On crée une nouvelle entrée dans la table profiles
            
            $sql = 'INSERT INTO `'.self::$table.'`
                (`login`,
                `password`,
                `email`,
                `rank`,
                `registration_key`,
                `registration_date`)
                VALUES
                (:login,
                :password,
                :email, 
                :rank,
                :registration_key,
                NOW()
                )';
            $query = Db::get()->prepare($sql);
            $query->bindValue(':login', $login, PDO::PARAM_STR);
            $query->bindValue(':password', User::hashpass($password), PDO::PARAM_STR);
            $query->bindValue(':email', $email, PDO::PARAM_STR);
            $query->bindValue(':rank', User::RANK_UNVALID, PDO::PARAM_INT);
            $registration_key = md5(microtime(TRUE)*100000);
            $query->bindValue(':registration_key', $registration_key, PDO::PARAM_STR);
            $query->execute();
            
            // Step 3 : On récupère les infos entrées dans l'objet.
            
            $user = new User();
            $sql = 'SELECT `profile_id`,
                           `login`,
                           `email`,
                           `rank`,
                           `registration_key`,
                           `registration_date`
                    FROM `'.self::$table.'`
                    WHERE `login` = :login';
            $query = Db::get()->prepare($sql);
            $query->bindValue(':login', $login, PDO::PARAM_STR);
            $query->setFetchMode(PDO::FETCH_INTO, $user);
            $query->execute();
            $query->fetch();
            
            // Step 4 : On complète la table users
            
            $sql = 'INSERT INTO `'.self::$userstable.'`
                    (`profile_id`, `username`, `useravatar`)
                    VALUES
                    (:profile_id, :username, \'./web/design/backgrounds/avatar/default.jpg\')';
            $query = Db::get()->prepare($sql);
            $query->bindValue(':profile_id', $user->profile_id, PDO::PARAM_INT);
            $query->bindValue(':username', $user->login, PDO::PARAM_STR);
            $query->execute();
            
            Model::commitTransaction();
            
            MsgQueue::addMessage('Inscription effectuée ! Il ne vous reste plus qu\'à cliquer sur le lien d\'activation qu\'on vous a envoyé par mail afin de finaliser la procédure.');

            // Step 5 : On envoie un e-mail de validation
            
            if(!User::sendValidationMail($user->login))
            {
                return false;
            }
            
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
        
        return true;
    }
    
    /**
     * @brief Envoie un mail de validation à l'utilisateur donc le login est $login.
     * @param in string $login Le login de l'utilisateur à qui envoyer le mail.
     * @return Vrai ou faux selon si l'opération a été effectuée avec succès.
     */
    public static function sendValidationMail($login)
    {
        try{
            Model::beginTransaction();
            $sql = 'SELECT `email`,
                        `registration_key`
                        FROM `'.self::$table.'`
                        WHERE `login` = :login';
            $query = Db::get()->prepare($sql);
            $query->bindValue(':login', $login, PDO::PARAM_STR);
            $query->execute();
            $res = $query->fetch(PDO::FETCH_ASSOC);
            
            Model::commitTransaction();
            
            if(!$res)
            {
                MsgQueue::addMessage('Login inexistant. L\'envoi du mail n\'a pas pu se faire.');
                return false;
            }
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
        
        try{
        
            $destinataire = $res['email'];
            $sujet = "Stupid Tribune - Activation de votre compte" ;
            $entete = "From: noreply@stupid_tribune.com" ;

            // Le lien d'activation est composé du login(log) et de la clé(cle)
            $message = 'Spleu ! Bienvenue sur Stupid Tribune, biatch !

            Pour activer votre compte, veuillez cliquer sur le lien ci dessous
            ou le copier/coller dans votre navigateur internet.

            '.Router::url('user/activate').'?log='.urlencode($login).'&cle='.urlencode($res['registration_key']).'


            ---------------
            Ceci est un mail automatique, Merci de ne pas y répondre.';


            mail($destinataire, $sujet, $message, $entete);

        }
        catch(Exception $e)
        {
            if(isdebug('db'))
            {
                echo 'Erreur Mail : ', $e->getMessage();
            }
            else
                MsgQueue::addMessage('Une erreur est survenue, veuillez réessayer.');
            return false;
        }
        
        return true;
    }
    
    /**
     * @brief Active le compte de l'utilisateur $login.
     * @param in string $login Le login de l'utilisateur à activer.
     * @param in string $key La clé d'enregistrement de l'utilisateur à valider.
     * @return Vrai ou faux selon si l'oprération a réussi.
     */
    public static function activate($login, $key)
    {
        try{
            Model::beginTransaction();
            
            // Etape 1 : on regarde si la clé est bonne
            
            $sql = 'SELECT `registration_key`, `profile_id`
                    FROM `'.self::$table.'`
                    WHERE `login` = :login';
            $query = Db::get()->prepare($sql);
            $query->bindValue(':login', $login, PDO::PARAM_STR);
            $query->execute();
            $res = $query->fetch(PDO::FETCH_ASSOC);
            
            if(!$res || $res['registration_key'] != $key)
            {
                MsgQueue::addMessage('Erreur de l\'activation du compte : mauvais login et/ou mauvaise clé.');
                return false;
            }
            
            //var_dump($res);
            
            // Etae 2 : on met à jour le profil
            
            $sql = 'UPDATE `'.self::$table.'`
                    SET `rank` = :rank
                    WHERE `profile_id` = :profile_id';
            $query = Db::get()->prepare($sql);
            $query->bindValue(':rank', User::RANK_MEMBER, PDO::PARAM_INT);
            $query->bindValue(':profile_id', $res['profile_id'], PDO::PARAM_INT);
            $query->execute();
            
            Model::commitTransaction();
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
        
        MsgQueue::addMessage('Votre compte a bien été actvé, vous pouvez vous connecter.');
        return true;
    }
    
}