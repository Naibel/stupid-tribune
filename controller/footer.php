<?php

class FooterController extends Controller
{
    
    public function defaultAction()
    {
        $this->display();
    }
    
    public function display()
    {
        Model::loadModel('article');
        Model::loadModel('comment');
        Model::loadModel('poll');
        
        $this->createView('display');
        
        // Articles
        $numarticles = 4;
        $articles = Article::getLastArticles($numarticles);
        $this->view->set('articles', $articles);
        
        // Comentaires
        $numcomments = 4;
        $comments = Comment::getLastComments($numcomments);
        foreach($comments as $comment)
        {
            $comment->comment_datetime = new DateTime($comment->comment_datetime);
            if(strlen($comment->comment_text) > 50)
                $comment->comment_text = substr($comment->comment_text, 0, 50).'...';
        }
        $this->view->set('comments', $comments);
        
        // Sondages
        $numpolls = 4;
        $polls = Poll::getLastPolls($numpolls, false);
        $this->view->set('polls', $polls);
        
    }
    
    
}