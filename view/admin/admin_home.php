<?php if(User::isLogged()){ ?>
	<span class="sous_titre" style="font-size:24px;">Bonjour <?php $this->pv('user_username'); ?> et bienvenue dans le ....<br/></span>
	<span class="sous_titre" style="font-size:32px;">Back-office !</span><br/><br/>
	<span class="sous_titre_2">... <br/><br/>Hum, hum... Pardon... Alors, que veux-tu faire ?</span><br/><br/><br/>

	<a href="<?php $this->pv('admin_question_url'); ?>">
		<span class="sous_titre_2">Ajouter une nouvelle question rigolote !</span>
	</a><br/><br/>

	<a href="<?php $this->pv('admin_answer_url'); ?>">
		<span class="sous_titre_2">Ajouter une nouvelle réponse à la con !</span>
	</a><br/><br/>

	<a href="<?php $this->pv('admin_poll_url'); ?>">
		<span class="sous_titre_2">Générer un nouveau sondage !</span>
	</a><br/><br/>

	<a href="<?php $this->pv('admin_article_url'); ?>"
		><span class="sous_titre_2">Rédiger un nouvel article !</span>
	</a><br/><br/><br/>

	<span style="font-size:18px; color:#550000;"><a href="<?php $this->pv('home_url'); ?>">Revenir à l'accueil</a></span>
<?php } else { ?>
    <span class="sous_titre_2">Vous devez être connecté pour acceder à cette partie du site.</span></br></br>
<?php } ?>