<?php if(User::isLogged()){ ?>
    <div style="width:100%; background-color:rgba(100,0,0,0.1); border-radius:10px;">
        <div style="padding:10px;">
            <span class="sous_titre_2">Un commentaire à écrire ? Allez-y !</span>
            <form action="<?php $this->pv('current_url'); ?>" method="post">
                <textarea placeholder="Contenu de votre commentaire (maximum 200 caractères)" required class="champ_blanc" style="height:100px;" name="commentsend_content" maxlength="200"></textarea>
                <input name="commentsend_article" type="hidden" value="<?php $this->pv('articleid') ?>">
                <input class="button" name="commentsend" value="Envoyer" type="submit">
            </form>
        </div>
    </div></br>
<?php } else { ?>
    <span class="sous_titre_2">Inscrivez-vous pour pouvoir commenter cet article !</span></br></br>
<?php } ?>