<?php

class Clock
{
    private $start;
    private $stop;
    private $diff;
    
    public function __construct($start = false)
    {
        if($start) $this->start();
    }
    
    public function start()
    {
        $this->start = microtime(true);
    }
    
    public function stop()
    {
        $this->stop = microtime(true);
        $this->diff = $this->stop - $this->start;
    }
    
    public function display($update = true)
    {
        if($update)
            $this->stop();
        echo '<div style="
                            display : inline-block;
                            background : #eee;
                            border : solid 1px #ddd;
                            padding : 4px;
                         "
              >
                  Page générée en ', round($this->diff, 6), ' secs
              </div>
        ';
    }
}