<?php if(User::isLogged()){ ?>
	<span class="sous_titre" style="font-size:24px;">Hey, <?php $this->pv('user_username'); ?> ! Tu désires générer un sondage ? Bah, vas-y !</span><br/><br/>

	<form action="<?php $this->pv('current_url'); ?>" method="post">

		<span class="sous_titre_2">Question du sondage :</span>

		<select class="champ_blanc" name="poll_question">
			<?php
				$list_questions=Admin::listQuestions();

				foreach ($list_questions as $key => $value) {
			?>
					<option value="<?php echo $list_questions[$key]['questiontext']; ?>"><?php echo $list_questions[$key]['questiontext']; ?></option> 
			<?php
				}
			?>
		</select>

		<input class="button" type="submit" name="pollsend" value="Envoyer"><br/><br/>

	</form>

	<span style="font-size:18px; color:#550000;"><a href="<?php $this->pv('home_url'); ?>">Revenir à l'accueil du back-office</a></span>
<?php } else { ?>
    <span class="sous_titre_2">Vous devez être connecté pour acceder à cette partie du site.</span></br></br>
<?php } ?>