<?php

class ErrorController extends Controller
{
    
    public function defaultAction()
    {
        $this->createView('errorUnknown');
    }
    
    public function error404()
    {
        $this->createView('error404');
        
    }
    
    
}