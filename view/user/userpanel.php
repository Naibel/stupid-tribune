<div class="formulaires_wrapper">
	<div class="formulaires_content">
        <div class="wrapper_membre_1">
            <div class="wrapper_content">
                <div class="wrapper_content_inner" style="text-align:left;">
                    <div class="image_profil">
                        <div style="padding:10px;">
                            <img src=<?php $this->pv('user_avatar'); ?> >
                        </div>
                    </div>
                    <div style="width:50%; float:left;">
                        <span style="font-size:22px;">Vous êtes <b><?php $this->pv('user_username'); ?></b></span><br/>
                        <span>Inscription<br/><div class="ligne_blanc" style="margin:2px 0px;"></div>
                            <b><?php echo $this->v('user_registration_date')->format('d/m/Y'); ?></b> à <b><?php echo $this->v('user_registration_date')->format('H:i'); ?></b>
                        </span><br/><br/>
                        <span>Dernière connexion<br/><div class="ligne_blanc" style="margin:2px 0px;"></div>
                            <b><?php echo $this->v('user_last_login')->format('d/m/Y'); ?></b> à <b><?php echo $this->v('user_last_login')->format('H:i'); ?></b>
                        </span><br/><br/>
                        <span>Nombre de sondages réalisés<br/><div class="ligne_blanc" style="margin:2px 0px;"></div>
                            <b><?php $this->pv('user_nbpolls'); ?></b>
                        </span><br/><br/>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper_membre_2">
            <div class="wrapper_content">
                <div class="wrapper_content_inner" style="text-align:left;">
                    <span>Changer de photo de profil ?</span><br/>
                    <form enctype="multipart/form-data" method="POST" action="<?php $this->pv('current_url'); ?>">
                        <input type="hidden" name="pseudo" value="<?php $this->pv('user_login'); ?>"/>
                        <input class="champ" type="file" placeholder="Votre fichier image" required	name="file_galerie" accept="image/*"/>
                        <input type="submit" class="button" name="uploadavatar" value="Uploader"/>
                    </form><br/><br/>
                    <form method="post" action="<?php $this->pv('current_url'); ?>">
                        <input class="button" type="submit" value="Déconnexion du profil" name="logout"/>
                    </form>
                </div>
            </div>
        </div>
	</div>
</div>

<div class="button_slide_wrapper">
	<div class="button_slide">
		<span class="btn_ins">
            Salut, <b><?php $this->pv('user_username'); ?></b> !
            <a id="btn_connexion" style="color:#fff;"><i style="color:#fff; margin-left:10px; font-size:10px;">Voir votre profil</i></a>
		</span>
	</div>
</div>