<!DOCTYPE html>

	<head>
		<meta charset="utf-8">
		<title>Stupid Tribune - Accueil</title>
		<link rel="stylesheet" href="<?php $this->pv('css_url'); ?>">
		<link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
		
	</head>

	<body>
        
        <?php $this->ps('userpanel'); ?>
        <?php $this->ps('header'); ?>
        <?php $this->ps('mainmenu'); ?>
        <?php $this->ps('msgqueue'); ?>
        
        <div class="content">
            <div class="colonne_1">
                <div class="colonne_wrapper">
                    <div class="colonne_inner_wrapper">
                        <?php $this->ps('content'); ?>
                    </div>
                </div>
            </div>
            
            <div class="colonne_2">
                <div class="colonne_wrapper">
                    <div class="colonne_inner_wrapper">
                        <?php $this->ps('searchform'); ?>
                        <?php $this->ps('currentpoll'); ?>
                    </div>
                </div>
            </div>
        </div>
        
        <?php $this->ps('footer'); ?>
        
        
        <!-- Javascript -->
        
        <script src ="<?php $this->pv('jquery_url'); ?>"></script>
		<script src ="<?php $this->pv('mainscript_url'); ?>"></script>
        
    </body>
</html>