<?php

class MsgqueueController extends Controller
{
    
    public function defaultAction()
    {
        $this->display();
    }
    
    public function display()
    {
        Model::loadModel('msgqueue');
        $this->createView('display');
    }
    
    
}