<?php

class HomeController extends Controller
{
    
    public function defaultAction()
    {
        $this->home();
    }
    
    public function home()
    {
        $this->createView('home');
        
        $this->request('recentarticles', 'article', 'recents');
        
        $this->setLayout('defaultLayout');
    }
    
    
}