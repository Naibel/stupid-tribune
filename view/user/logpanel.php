<div class="formulaires_wrapper">
	<div class="formulaires_content">
        <div class="wrapper">
            <div class="wrapper_content">
                <div class="wrapper_content_inner">
                    <span style="font-size:24px;">Pas encore inscrit ?</span><br/>
                    <span>Inscrivez-vous sur le site pour profiter de toutes ses fonctionnalités fonctionnelles !</span>
                    <div class="ligne_blanc"></div>
                    <form method="post" action="<?php $this->pv('current_url'); ?>">
                        <input class="champ" type="text" name="register_login" required placeholder="Votre pseudo" maxlength="64"></input>
                        <input class="champ" type="password" name="register_password" required placeholder="Votre mot de passe" maxlength="128"></input>
                        <input class="champ" type="email" name="register_email" required placeholder="Votre e-mail" maxlength="64"></input>
                        <input class="button" type="submit" name="register" value="S'inscrire"></input>
                    </form>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <div class="wrapper_content">
                <div class="wrapper_content_inner">
                    <span style="font-size:24px;">Déjà membre ?</span><br/>
                    <span>Bah, qu'est-ce que t'attends pour te connecter, pleutre ?</span>
                    <div class="ligne_blanc"></div>
                    <form method="post" action="<?php $this->pv('current_url'); ?>">
                        <input class="champ" type="text" name="login_login" required placeholder="Votre pseudo" maxlength="64"></input>
                        <input class="champ" type="password" name="login_password" required placeholder="Votre mot de passe" maxlength="128"></input>
                        <input class="button" type="submit" name="login" value="Se connecter"></input>
                    </form>
                </div>
            </div>
        </div>
	</div>
</div>

<div class="button_slide_wrapper">
	<div class="button_slide">
		<span class="btn_ins">
            <a id="btn_connexion">Inscription/Connexion</a>
		</span>
	</div>
</div>