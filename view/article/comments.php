<span class="sous_titre">Commentaires</span>
<div class="ligne"></div>

<?php if(empty($this->v('comments'))){ ?>
    <span class="sous_titre_2">Pas de commentaires ... pour l'instant !</span></br></br>
<?php } else { ?>
    <?php foreach($this->v('comments') as $comment){ ?>
        <div style="overflow:hidden; clear:both;">
            <div class="colonne_1_comment">
                <div class="colonne_wrapper">
                    <div class="colonne_inner_wrapper">
                        <span class="sous_titre_2"><?php echo $comment->comment_author; ?></span></br>
                        <span><?php echo $comment->comment_datetime->format('d/m/Y - H:i'); ?></span></br>
                    </div>
                </div>
            </div>
            <div class="colonne_2_comment">
                <div class="colonne_wrapper">
                    <div class="colonne_inner_wrapper" style="background-color:rgba(100,0,0,0.3); box-shadow: 0px 0px 5px rgba(0,0,0,0.0); margin:5px 0px; ">
                        <span><?php echo $comment->comment_text; ?></span>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>