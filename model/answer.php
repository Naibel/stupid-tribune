<?php

class Answer extends Model
{
    public static $table = 'answers';
    public static $cattable = 'answercategories';
    
    public $answerid;
    public $answertext;
    public $answernum;
    
    public function __construct($content = null)
    {
        parent::__construct();
    }
    
    /**
     * @brief Retourne un objet Patate récupéré depuis la BDD par son id.
     * @param in int $id L'id de la patate à récupérer.
     * @return retourne la Patate récupérée, null sinon.
     */
    public static function getById($id)
    {
        // TODO
    }
    
    public static function getRandoms($nb)
    {
        try{
            Model::beginTransaction();
            
            $ret = array();
            
            // Pick random
            // OH PUTAIN DE MERDE C'TE REQUETE DE FOU !!!!
            // YAYAYAYAYAYAYAYYAYAYAYYAYAAYAYYAYAYAYAY !!!!!!!
            // PUTAIN PUTAIN PUTAIN PUTAIN PUTAIN PUTAIN PUTAIN !
            $sql = 'SELECT r1.`answerid`,
                    r1.`answertext`
                    FROM `'.self::$table.'` AS r1 JOIN
                        (SELECT (RAND() *
                            (SELECT MAX(`'.self::$table.'`.`answerid`)
                            FROM `'.self::$table.'`)) AS id)
                        AS r2
                        
                        INNER JOIN `'.self::$cattable.'`
                        ON r1.`answerid` = `'.self::$cattable.'`.`answerid`
                        
                    WHERE `'.self::$cattable.'`.`categoryid` IN (2, 4)
                    AND r1.`answerid` >= r2.id
                    ORDER BY r1.`answerid` ASC
                    LIMIT 1';
            $req = Db::get()->prepare($sql);
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Answer', array());
            
            for($i=0 ; $i<$nb ; ++$i)
            {
                $req->execute();
                $ret[] = $req->fetch();
            }
            
            //var_dump($ret);
            
            Model::commitTransaction();
            
            return $ret;
            

        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
    public static function getByPoll($id, $content = true)
    {
        try{
            Model::beginTransaction();
            
            $sql = 'SELECT * FROM `answers`
                    LEFT JOIN `pollanswers`
                        ON `answers`.`answerid` = `pollanswers`.`answerid`
                    WHERE `pollanswers`.`pollid` = :id
                    ORDER BY `pollanswers`.`answernum`';
            $req = Db::get()->prepare($sql);
            $req->bindValue(':id', $id, PDO::PARAM_INT);
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Answer', array());
            $req->execute();
            $res = $req->fetchAll();
            
            Model::commitTransaction();
            
            return $res;
            
        }
        catch(Exception $e)
        {
            Model::BDDError($e);
            return false;
        }
    }
    
}