<?php
/**
 * @brief Le Dispatcher est la classe principale de l'application
 * Son constructeur, le point d'entrée de l'application, suffit à rendre une page construite entièrement.
 */


class Dispatcher
{
    private $request;        //*< L'url parsée de l'utilisateur
    private $controller;     //*< Le controlleur principal, déterminé par le router
    private $action;         //*< L'action que devra réaliser le controlleur, déterminé par le router
    
    const ERROR404 = 404;
    const ERROR403 = 403;
    
    /**
     * @brief Le point d'entrée de l'application. C'est également le point de sortie.
     * @return Cette fonction ne retroune rien.
     */
    function __construct()
    {
        // Step 0 : On effectue les pré-taches à faire ($_POST)
        $this->doTasks();
        
        // Step 1 : On parse la requête
        $this->request = Request::getClientRequest();
        Router::parse($this->request);
        
        $this->launch($this->request);
        
        // Step 4 : On affiche la vue.
        if($this->controller->layout)
            $this->controller->layout->printView();
        else
            $this->controller->printView();
    }
    
    public function launch($req = null)
    {        
        // Step 2 : On crée le controlleur demandé
        if($req)
        {
            if(!$req->controller)
            {
                $this->controller = Dispatcher::loadController(DEFAULT_CONTROLLER, array());
            }
            else
            {
                $this->controller = Dispatcher::loadController($req->controller, $req->params);
            }
        }
        
        if(!$this->controller)
        {
            $this->notFound();
        }
        
        // Step 3 : On exécute l'action demandée
        if($req && !$this->action) $this->action = $req->action;
        if(!$this->action) $this->action = DEFAULT_ACTION;
        if(!method_exists($this->controller, $this->action) or method_exists('Controller', $this->action))
        {
            $this->notFound();
        }
            
        $error = call_user_func(
            array($this->controller, $this->action)
        );
        
        switch($error)
        {
        case self::ERROR404:
            $this->notFound();
            break;
        default:
            break;
        }
            
            
    }
    
    /**
     * @brief Charge le controlleur $ctrl avec les paramètres $params et le retourne.
     * @param in string $ctrl Le nom du controlleur à charger, en minuscule. Correspond au nom du fichier sans l'extension.
     * @param in array $params Un tableau deparamètres à envoyer au controlleur. Celui-ci saura quoi en faire.
     * @return Retourne le controlleur créé, false s'il n'a pas pu être créé.
     */
    public static function loadController($ctrl, $params)
    {
        $file = ROOT . CONTROLLER_DIR . $ctrl . '.php';
        if(!file_exists($file)) return false;
        $name = ucfirst($ctrl) . 'Controller';
        require_once $file;
        $ret = new $name();
        $ret->name = $ctrl;
        $ret->params = $params;
        return $ret;
    }
    
    /**
     * @brief Charge le controlleur 404 à la palce du controlleur demandé.
     * @return Cette fonction ne retroune rien.
     */
    public function notFound()
    {
        $this->controller = $this->loadController(
            ERROR_CONTROLLER,
            array()
        );
        $this->action = ACTION_404;
        $this->launch();
    }
    
    public function doTasks()
    {
        foreach($_POST as $k => $v)
        {
            $t = Router::getTask($k);
            if(!$t) continue;
            $req = new Request();
            $req->url = $t;
            Router::parse($req);
            //debugn('YA1TASK !!', $req);
            $this->launch($req);
            $this->clear();
        }
            
    }
    
    public static function getTask($t)
    {
        return Router::getTask($k);
    }
    
    public function clear()
    {
        $this->request = null;
        $this->controller = null;
        $this->action = null;
    }
    
    const PARAM_ALL = 0;
    const PARAM_INT = 10;
    const PARAM_STR = 20;
    
    
    public static function inputPost($name, $type = PARAM_ALL)
    {
        if(!isset($_POST[$name]) or empty($_POST[$name]))
            return null;
        
        switch($type)
        {
        case self::PARAM_ALL:
            return $_POST[$name];
            break;
        case self::PARAM_INT:
            if(!ctype_digit($_POST[$name])) return null;
            break;
        case self::PARAM_STR:
            break;  // _POST variables are always strings.
        default :
            break;
        }
        return $_POST[$name];
    }
    
    public static function inputGet($name, $type = PARAM_ALL)
    {
        if(!isset($_GET[$name]) or empty($_GET[$name]))
            return null;
        
        switch($type)
        {
        case self::PARAM_ALL:
            return $_GET[$name];
            break;
        case self::PARAM_INT:
            if(!ctype_digit($_GET[$name])) return null;
            break;
        case self::PARAM_STR:
            break;  // _POST variables are always strings.
        default :
            break;
        }
        return $_GET[$name];
    }
}

