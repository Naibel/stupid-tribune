<?php

class HeaderController extends Controller
{
    
    public function defaultAction()
    {
        $this->display();
    }
    
    public function display()
    {
        Model::loadModel('answer');
        $this->createView('display');
        
        $this->view->set('home_url', Router::url(''));
        
        $this->view->set('admin_url', Router::url('admin'));

        $this->view->set('answers', Answer::getRandoms(4));
        
    }
    
    
}