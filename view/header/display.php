<header>
  	<div class="header_wrapper">
  		<div class="header_wrapper_inner">
		  	<a href="<?php $this->pv('home_url') ?>" style="text-decoration:none;">
                <span class="title" style="font-style:italic;"><?php echo $this->v('answers')[0]->answertext; ?> </span>
                <span class="title" style="font-weight:300;"> Tribune</span></a>
		   	<div class="ligne"></div>
		   	<span class="citation">"Sans la liberté de <?php echo $this->v('answers')[1]->answertext; ?>, il n'est point de <?php echo $this->v('answers')[2]->answertext; ?> flatteur."</span><br/>
		   	<span><?php echo $this->v('answers')[3]->answertext; ?></span>
		</div>
	</div>
</header>